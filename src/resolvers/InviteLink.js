const InviteToJointRetro = {
    retro: ({ id }, args, context) => {
        return context.prisma.inviteToJointRetro({ id }).retro()
    },
    
}

module.exports = {
    InviteToJointRetro,
}
