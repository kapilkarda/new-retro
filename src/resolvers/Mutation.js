const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const {
    userInviteForRetroJoin
} = require('../utils/retroInvite');
const {
    verifyEmail
} = require('../utils/verifyEmail');

const Mutation = {
    // User Auth
    signupUser: async (parent, args, context) => {
        var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (args.email !== '' && args.password !== '') {
            if (reg.test(args.email)) {
                const foundEmail = await context.prisma.user({
                    email: args.email
                });
                if (foundEmail) {
                    throw new Error(`Such user email: ${args.email} already exist`);
                }
                if (args.userType === 'Bussiness_Owner' || args.userType === 'Freelance') {

                    console.log(args.email);
                    const hashPassword = await bcrypt.hash(args.password, 10);
                    const userRespo = await context.prisma.createUser({
                        email: args.email,
                        firstName: args.firstName,
                        lastName: args.lastName,
                        userStatus: args.userStatus,
                        role: args.role,
                        isCompanyAdmin: 'true',
                        isRetroAdmin: 'true',
                        password: hashPassword
                    });

                    const userCompany = await context.prisma.createCompany({
                        companyName: args.companyName,
                        user: {
                            connect: {
                                id: userRespo.id
                            }
                        }
                    })

                    const updateUser = await context.prisma.updateUser({
                        where: {
                            id: userRespo.id
                        },
                        data: {
                            companyId: userCompany.id
                        }
                    });


                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'lalit.emaster@gmail.com',
                            pass: 'Lalit001'
                        }
                    });

                    const mailOptions = verifyEmail(updateUser.id, updateUser.email);

                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });


                    return updateUser;
                } else {

                    if (args.companyId === '' || typeof(args.companyId) === 'undefined') {
                        throw new Error(`companyId is mendatory`);
                    }

                    if(args.isCompanyAdmin === 'true'){
                        const hashPassword = await bcrypt.hash(args.password, 10);
                        const userData = await context.prisma.createUser({
                            email: args.email,
                            firstName: args.firstName,
                            lastName: args.lastName,
                            userStatus: args.userStatus,
                            role: args.role,
                            password: hashPassword,
                            companyId: args.companyId,
                            isGuest: 'true',
                            isCompanyAdmin: args.isCompanyAdmin
                        });

                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'lalit.emaster@gmail.com',
                                pass: 'Lalit001'
                            }
                        });

                        const mailOptions = verifyEmail(userData.id, userData.email);

                        transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                        return userData;

                    }else {
                        const hashPassword = await bcrypt.hash(args.password, 10);
                        const userData = await context.prisma.createUser({
                            email: args.email,
                            firstName: args.firstName,
                            lastName: args.lastName,
                            userStatus: args.userStatus,
                            role: args.role,
                            password: hashPassword,
                            companyId: args.companyId,
                            isGuest: 'true'
                        });

                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'lalit.emaster@gmail.com',
                                pass: 'Lalit001'
                            }
                        });

                        const mailOptions = verifyEmail(userData.id, userData.email);

                        transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                        return userData;
                    }    

              
                }
            }
            throw new Error(`This email is not valid.`);
        }
        throw new Error(`Email and password should not empty`);

    },

    login: async (parent, {
        email,
        password
    }, context) => {
        const user = await context.prisma.user({
            email: email
        });
        if (!user) {
            throw new Error(`No such user found for email: ${email}`)
        }

        if (user.userStatus === '0' || user.userStatus === null) {
            throw new Error(`Such user is not actived`)
        }

        if (user.isEmailVerify === false || user.isEmailVerify === null) {
            throw new Error(`Please verify email`)
        }



        const valid = await bcrypt.compare(password, user.password);
        if (!valid) {
            throw new Error('Invalid password')
        }
        return user
    },

    updateUser: async (parent, args, context) => {
        const updateUser = await context.prisma.updateUser({
            where: {
                id: args.id
            },
            data: {
                firstName: args.firstName,
                lastName: args.lastName,
                userStatus: args.userStatus,
                companyName: args.companyName,
                addressLine1: args.addressLine1,
                addressLine2: args.addressLine2,
                city: args.city,
                state: args.state,
                zipCode: args.zipCode,
                phoneNumber: args.phoneNumber,
                companyId: args.companyId,
                isCompanyAdmin: args.isCompanyAdmin,
                isEmailVerify: args.isEmailVerify,
                role: args.role,
                accountType: args.accountType,
                promoCodeIsUsed: args.promoCodeIsUsed,
                isRetroAdmin: args.isRetroAdmin,
                isGuest: args.isGuest,
                hideArchived:args.hideArchived
            }
        });
        return updateUser;
    },

    changePassword: async (root, args, context) => {

        const user = await context.prisma.user({
            id: args.userId
        });

        if (user) {

            const valid = await bcrypt.compare(args.oldPwd, user.password);

            if (!valid) {
                throw new Error('Password not match');
            }
            const hashPassword = await bcrypt.hash(args.newPwd, 10);
            return await context.prisma.updateUser({
                where: {
                    id: user.id
                },
                data: {
                    password: hashPassword
                }
            });

        }
        throw new Error('User not found');
    },


    singleUserDelete: async (root, args, context) => {

        return await context.prisma.deleteUser({
            id: args.id
        });

        return await context.prisma.updateUser({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },

    // Default Project
    createDefaultProject: async (parent, args, context) => {
        const defaultProject = await context.prisma.createdefaultProject({
            projectname: args.projectname,
            createdBy: {
                connect: {
                    id: args.createdBy
                }
            }
        });
        return defaultProject;
    },

    deleteProject: async (parent, args, context) => {
        return await context.prisma.updatedefaultProject({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"
            }
        });
    },

    // Default Template
    createDefaultTemplate: async (parent, args, context) => {
        return await context.prisma.createdefaultTemplate({
            templatename: args.templatename,
            category1: args.category1,
            category2: args.category2,
            category3: args.category3,
            category4: args.category4,
            archive: args.archive
        });
    },

    updateDefaultTemplate: async (parent, args, context) => {
        return await context.prisma.updatedefaultTemplate({
            where: {
                id: args.defaultTempId
            },
            data: {
                templatename: args.templatename,
                category1: args.category1,
                category2: args.category2,
                category3: args.category3,
                category4: args.category4,
                isDeleted: args.isDeleted,
                archive: args.archive
            }
        });
    },

    deleteDefaultTemplate: async (parent, args, context) => {
        return await context.prisma.updatedefaultTemplate({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },

    // Default User Template
    createUserTemplate: async (parent, args, context) => {
        return await context.prisma.createuserTemplate({
            templatename: args.templatename,
            category1: args.category1,
            category2: args.category2,
            category3: args.category3,
            category4: args.category4,
            userid: {
                connect: {
                    id: args.userid
                }
            }
        });
    },

    updateUserTemplate: (parent, args, context) => {
        const updateUser = context.prisma.updateuserTemplate({
            where: {
                id: args.templateId
            },
            data: {
                archive: args.archive,
                templatename: args.templatename,
                category1: args.category1,
                category2: args.category2,
                category3: args.category3,
                category4: args.category4,
            }
        });
        return updateUser;
    },

    deleteTemplate: async (parent, args, context) => {
        return await context.prisma.updateuserTemplate({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"
            }
        });
    },

    //  Retro
    createRetro: async (parent, args, context) => {

        if (args.projectId === '' || typeof(args.projectId) === 'undefined') {

            const defaultProject = await context.prisma.createdefaultProject({
                projectname: args.projectname,
                createdBy: {
                    connect: {
                        id: args.userid
                    }
                }
            });

            var startdate = new Date();
            var enddate = new Date();

            const defaultTemplate = await context.prisma.defaultTemplate({
                id: args.templateId
            });

            if (defaultTemplate) {
                await context.prisma.updateUser({
                    where: {
                        id: args.userid
                    },
                    data: {
                        isRetroAdmin: 'true'
                    }
                });

                return await context.prisma.createRetro({

                    templatename: args.templatename,
                    sprintnumber: args.sprintnumber,
                    startDate: startdate,
                    endDate: enddate,
                    startTime: args.startTime,
                    endTime: args.endTime,
                    activity: args.activity,
                    projectId: defaultProject.id,
                    projects: {
                        connect: {
                            id: defaultProject.id
                        }
                    },
                    templateId: args.templateId,
                    roomCode: args.roomCode,
                    retrocategory1: args.retrocategory1,
                    retrocategory2: args.retrocategory2,
                    retrocategory3: args.retrocategory3,
                    retrocategory4: args.retrocategory4,
                    retroAdmin: args.retroAdmin,
                    notes: args.notes,
                    userid: {
                        connect: {
                            id: args.userid
                        }
                    },
                    repeatEvery: args.repeatEvery,
                    inputWeek: args.inputWeek,
                    shareLink: args.shareLink,
                    isSummary: args.isSummary
                });
            } else {

                await context.prisma.updateUser({
                    where: {
                        id: args.userid
                    },
                    data: {
                        isRetroAdmin: 'true'
                    }
                });
                var startdate = new Date();
                var enddate = new Date();

                return await context.prisma.createRetro({

                    templatename: args.templatename,
                    sprintnumber: args.sprintnumber,
                    startDate: startdate,
                    endDate: enddate,
                    startTime: args.startTime,
                    endTime: args.endTime,
                    activity: args.activity,
                    projectId: defaultProject.id,
                    projects: {
                        connect: {
                            id: defaultProject.id
                        }
                    },
                    templateId: args.templateId,

                    userTemplate: {
                        connect: {
                            id: args.templateId
                        }
                    },
                    roomCode: args.roomCode,
                    retrocategory1: args.retrocategory1,
                    retrocategory2: args.retrocategory2,
                    retrocategory3: args.retrocategory3,
                    retrocategory4: args.retrocategory4,
                    retroAdmin: args.retroAdmin,
                    notes: args.notes,
                    userid: {
                        connect: {
                            id: args.userid
                        }
                    },
                    repeatEvery: args.repeatEvery,
                    inputWeek: args.inputWeek,
                    shareLink: args.shareLink,
                    isSummary: args.isSummary
                });
            }


        } else {

            var startdate = new Date();
            var enddate = new Date();

            const defaultTemplate = await context.prisma.defaultTemplate({
                id: args.templateId
            });

            if (defaultTemplate) {

                await context.prisma.updateUser({
                    where: {
                        id: args.userid
                    },
                    data: {
                        isRetroAdmin: 'true'
                    }
                });

                return await context.prisma.createRetro({
                    templatename: args.templatename,
                    sprintnumber: args.sprintnumber,
                    startDate: startdate,
                    endDate: enddate,
                    startTime: args.startTime,
                    endTime: args.endTime,
                    activity: args.activity,
                    projectId: args.projectId,
                    projects: {
                        connect: {
                            id: args.projectId
                        }
                    },
                    templateId: args.templateId,
                    roomCode: args.roomCode,
                    retrocategory1: args.retrocategory1,
                    retrocategory2: args.retrocategory2,
                    retrocategory3: args.retrocategory3,
                    retrocategory4: args.retrocategory4,
                    retroAdmin: args.retroAdmin,
                    notes: args.notes,
                    userid: {
                        connect: {
                            id: args.userid
                        }
                    },
                    repeatEvery: args.repeatEvery,
                    inputWeek: args.inputWeek,
                    shareLink: args.shareLink,
                    isSummary: args.isSummary
                });
            } else {

                var startdate = new Date();
                var enddate = new Date();

                await context.prisma.updateUser({
                    where: {
                        id: args.userid
                    },
                    data: {
                        isRetroAdmin: 'true'
                    }
                });
                return await context.prisma.createRetro({
                    templatename: args.templatename,
                    sprintnumber: args.sprintnumber,
                    startDate: startdate,
                    endDate: enddate,
                    startTime: args.startTime,
                    endTime: args.endTime,
                    activity: args.activity,
                    projectId: args.projectId,
                    projects: {
                        connect: {
                            id: args.projectId
                        }
                    },
                    templateId: args.templateId,
                    userTemplate: {
                        connect: {
                            id: args.templateId
                        }
                    },
                    roomCode: args.roomCode,
                    retrocategory1: args.retrocategory1,
                    retrocategory2: args.retrocategory2,
                    retrocategory3: args.retrocategory3,
                    retrocategory4: args.retrocategory4,
                    retroAdmin: args.retroAdmin,
                    notes: args.notes,
                    userid: {
                        connect: {
                            id: args.userid
                        }
                    },
                    repeatEvery: args.repeatEvery,
                    inputWeek: args.inputWeek,
                    shareLink: args.shareLink,
                    isSummary: args.isSummary
                });
            }
        }
    },

    updateRetro: async (parent, args, context) => {
        if (args.isEditRetro == "true") {
            if ((args.projectId === '' || typeof(args.projectId) === 'undefined') && args.projectname !== '' && typeof(args.projectname) !== 'undefined') {

                const defaultProject = await context.prisma.createdefaultProject({
                    projectname: args.projectname,
                    createdBy: {
                        connect: {
                            id: args.userid
                        }
                    }
                });

                return await context.prisma.updateRetro({
                    where: {
                        id: args.retroId
                    },
                    data: {
                        isSummary: args.isSummary,
                        templatename: args.templatename,
                        sprintnumber: args.sprintnumber,
                        startDate: args.startDate,
                        endDate: args.endDate,
                        startTime: args.startTime,
                        endTime: args.endTime,
                        activity: args.activity,
                        projectId: defaultProject.id,
                        projects: {
                            connect: {
                                id: defaultProject.id
                            }
                        },
                        templateId: args.templateId,
                        roomCode: args.roomCode,
                        retrocategory1: args.retrocategory1,
                        retrocategory2: args.retrocategory2,
                        retrocategory3: args.retrocategory3,
                        retrocategory4: args.retrocategory4,
                        retroAdmin: args.retroadmin,
                        published: args.published,
                        timer: args.timer,
                        notes: args.notes,
                        isTimerStatus: args.isTimerStatus,
                        repeatEvery: args.repeatEvery,
                        inputWeek: args.inputWeek,
                        shareLink: args.shareLink,
                        isDeleted: args.isDeleted
                    }
                });

            } else {


                if (args.projectId !== '' && typeof(args.projectname) !== 'undefined') {

                    const defaultProject = await context.prisma.updatedefaultProject({
                        where: {
                            id: args.projectId
                        },
                        data: {
                            projectname: args.projectname
                        }
                    });
                }

                return await context.prisma.updateRetro({
                    where: {
                        id: args.retroId
                    },
                    data: {
                        isSummary: args.isSummary,
                        templatename: args.templatename,
                        sprintnumber: args.sprintnumber,
                        startDate: args.startDate,
                        endDate: args.endDate,
                        startTime: args.startTime,
                        endTime: args.endTime,
                        activity: args.activity,
                        templateId: args.templateId,
                        roomCode: args.roomCode,
                        retrocategory1: args.retrocategory1,
                        retrocategory2: args.retrocategory2,
                        retrocategory3: args.retrocategory3,
                        retrocategory4: args.retrocategory4,
                        retroAdmin: args.retroAdmin,
                        published: args.published,
                        timer: args.timer,
                        notes: args.notes,
                        isTimerStatus: args.isTimerStatus,
                        repeatEvery: args.repeatEvery,
                        inputWeek: args.inputWeek,
                        shareLink: args.shareLink,
                        isDeleted: args.isDeleted
                    }
                });
            }
        } else {


            if (args.isEditRetro != "true") {

                return await context.prisma.updateRetro({
                    where: {
                        id: args.retroId
                    },
                    data: {

                        isSummary: args.isSummary,
                        templatename: args.templatename,
                        sprintnumber: args.sprintnumber,
                        startDate: args.startDate,
                        endDate: args.endDate,
                        startTime: args.startTime,
                        endTime: args.endTime,
                        activity: args.activity,
                        templateId: args.templateId,
                        projectId: args.projectId,
                        roomCode: args.roomCode,
                        retrocategory1: args.retrocategory1,
                        retrocategory2: args.retrocategory2,
                        retrocategory3: args.retrocategory3,
                        retrocategory4: args.retrocategory4,
                        retroAdmin: args.retroAdmin,
                        published: args.published,
                        timer: args.timer,
                        notes: args.notes,
                        isTimerStatus: args.isTimerStatus,
                        repeatEvery: args.repeatEvery,
                        inputWeek: args.inputWeek,
                        shareLink: args.shareLink,
                        isSummary: args.isSummary,
                        isDeleted: args.isDeleted
                    }
                });
            }


        }
    },

    deleteRetro: async (parent, args, context) => {
        return await context.prisma.deleteRetro({
            id: args.id
        });

        return await context.prisma.updateRetro({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },

    updateRetro1: async (parent, args, context) => {
        return await context.prisma.updateRetro1({
            where: {
                id: args.retroId
            },
            data: {
                activity: args.activity,
                roomCode: args.roomCode

            }
        });
    },


    startRetro: async (parent, args, context) => {
        const responseData = await context.prisma.retro({
            id: args.retroId
        });
        if (responseData.startedBy == null) {
            return await context.prisma.updateRetro({
                where: {
                    id: args.retroId
                },
                data: {
                    startedBy: args.userId,
                    isTimerStatus: "Pending",
                    timer: "18000"
                }
            });
        } else {
            return responseData;
        }
    },

    // Invite User To Join Retro
    createInvite: async (parent, {
        retroId,
        email,
        roomCode,
        isAdmin,
        startDate,
        endDate,
        startTime,
        endTime,
        inputWeek,
        repeatEvery,
        shareLink
    }, context) => {

        let roomCodeMail = roomCode;
        const inviteJoin = await context.prisma.inviteToJointRetroes({
            where: {
                retroId: retroId,
                email: email
            }
        });
        if (inviteJoin.length == 0) {
            const inviteResponse = await context.prisma.createInviteToJointRetro({
                retroId,
                roomCode,
                startDate,
                endDate,
                startTime,
                endTime,
                isAdmin,
                inputWeek,
                repeatEvery,
                shareLink,
                email,
                url: 'http://45.61.48.46:3006/startretro/' + roomCode + '/' + retroId,
                retro: {
                    connect: {
                        id: retroId
                    }
                }
            });

            // get project Info to send mail
            const retroData = await context.prisma.retro({
                id: retroId
            });
            let sprint, projectName, startData
            if (retroData) {
                sprint = retroData.sprintnumber;
                startData = retroData.startData;
                const projectData = await context.prisma.defaultProject({
                    id: retroData.projectId
                });
                projectName = projectData.projectname
            }


            return inviteResponse
        } else {
            return inviteJoin[0];
        }
    },


    updateInvite: async (parent, args, context) => {
        const responseData = await context.prisma.updateInviteToJointRetro({
            where: {
                id: args.inviteId
            },
            data: {
                isAdmin: args.isAdmin,
                email: args.email,
                startDate: args.startDate,
                endDate: args.endDate,
                startTime: args.startTime,
                endTime: args.endTime,
                url: args.url,
                inputWeek: args.inputWeek,
                repeatEvery: args.repeatEvery,
                roomCode: args.roomCode,
                shareLink: args.shareLink,
                isJoined: args.isJoined
            }
        });
        return responseData;
    },

    updateIsJoinFieldInvite: async (parent, args, context) => {
        const inviteJoin = await context.prisma.inviteToJointRetroes({
            where: {
                retroId: args.retroId,
                email: args.email
            }
        });

        if (inviteJoin.length !== 0) {

            const responseData = await context.prisma.updateInviteToJointRetro({
                where: {
                    id: inviteJoin[0].id
                },
                data: {
                    isJoined: args.isJoined
                }
            });

            if (responseData) {
                if (args.isJoined === 'true') {
                    await context.prisma.updateUser({
                        where: {
                            email: args.email
                        },
                        data: {
                            isRetroAdmin: responseData.isAdmin
                        }
                    });
                }
            }

            return responseData;

        }
        throw new Error(`No found`);

    },

    deleteInvite: async (parent, args, context) => {

        return await context.prisma.deleteInviteToJointRetro({
            id: args.id
        });

    },

    //Thoughts
    createThought: async (parent, args, context) => {
        return await context.prisma.createThought({
            thought: args.thought,
            isLike: args.isLike,
            category: args.category,
            groupId: args.groupId,
            groupName: args.groupName,
            retroid: {
                connect: {
                    id: args.retroid
                }
            },
            userid: {
                connect: {
                    id: args.userid
                }
            }
        });
    },

    updateThought: async (parent, args, context) => {
        return await context.prisma.updateThought({
            where: {
                id: args.thoughtId
            },
            data: {
                thought: args.thought,
                category: args.category,
                isLike: args.isLike,
                groupId: args.groupId,
                groupName: args.groupName
            }
        });
    },

    // when isEdit is true means we edit thought and when its false it means we like thought
    updateLikeThought: async (parent, args, context) => {
        if (args.isEdit == false || args.isEdit == "false") {
            const thoughtsData = await context.prisma.thoughts({
                where: {
                    id: args.thoughtId
                }
            });
            if (thoughtsData[0].Like) {
                var LikesData = JSON.parse(thoughtsData[0].Like);
                var deleteIndex = 0;
                for (var i = 0; i < LikesData.length; i++) {
                    if (LikesData[i] == args.userId) {
                        deleteIndex = i + 1;
                        break;
                    }
                }
                if (deleteIndex == 0) {
                    LikesData.push(args.userId);
                } else {
                    LikesData.splice(deleteIndex - 1, 1);
                }
                return await context.prisma.updateThought({
                    where: {
                        id: args.thoughtId
                    },
                    data: {
                        Like: JSON.stringify(LikesData)
                    }
                });
            } else {
                var LikeData = [args.userId];
                return await context.prisma.updateThought({
                    where: {
                        id: args.thoughtId
                    },
                    data: {
                        Like: JSON.stringify(LikeData)
                    }
                });
            }
        } else {
            return await context.prisma.updateThought({
                where: {
                    id: args.thoughtId
                },
                data: {
                    thought: args.thought
                }
            });
        }
    },

    deleteThought: async (root, args, context) => {
        return await context.prisma.deleteThought({
           id: args.id
        })
    },

    //Company
    createCompany: async (parent, args, context) => {
        return await context.prisma.createCompany({
            companyName: args.companyName,
            companyId: args.companyId,

        });
    },

    updateCompany: async (parent, args, context) => {
        return await context.prisma.updateCompany({
            where: {
                id: args.id
            },
            data: {
                companyName: args.companyName,
                isMonthly: args.isMonthly,
                billingId: args.billingId,
                amount: args.amount,
                lastPayment: args.lastPayment,
                successOrFail: args.successOrFail
            }
        });
    },

    deleteCompany: async (parent, args, context) => {
        return await context.prisma.updateCompany({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },

    // Billing Summary
    createBillingSummary: async (parent, args, context) => {

        if (typeof(args.promoCodeId) !== 'undefined' && args.promoCodeId !== '') {

            const foundPromoCode = await context.prisma.billingSummaries({
                where: {
                    AND: [{
                        companyId: args.companyId
                    }, {
                        promoCodeId: args.promoCodeId
                    }]
                }
            });


            if (foundPromoCode.length === 0) {
                const billingSummaryRes = await context.prisma.createBillingSummary({
                    planId: args.planId,
                    whatsIncluded: args.whatsIncluded,
                    promoCodeId: args.promoCodeId,
                    companyId: args.companyId,
                    paymentMethodId: args.paymentMethodId,
                    billingContact: args.billingContact,
                    directPay: args.directPay,
                    userId: args.userId,
                    user: {
                        connect: {
                            id: args.userId
                        }
                    }

                });

                if (billingSummaryRes) {


                    const responsePlan = await context.prisma.plan({
                        id: billingSummaryRes.planId
                    });

                    let finalAmount = responsePlan.amount;

                    const promoCodeRes = await context.prisma.promoCode({
                        id: billingSummaryRes.promoCodeId
                    });

                    let discount = Number(promoCodeRes.discountValue);
                    discount = finalAmount * (discount / 100);
                    finalAmount -= discount;

                    const updateBill = await context.prisma.updateBillingSummary({
                        where: {
                            id: billingSummaryRes.id
                        },
                        data: {
                            finalAmount: finalAmount,
                            directPay: args.directPay
                        }
                    });

                    if (updateBill) {
                        const lastPayment = new Date();
                        let successOrFail;
                        switch (updateBill.directPay) {
                            case 'on':
                                successOrFail = 'true';
                                break;
                            case 'off':
                                successOrFail = 'false';
                                break;
                            default:
                                successOrFail = 'false';
                        }

                        await context.prisma.updateCompany({
                            where: {
                                id: updateBill.companyId
                            },
                            data: {
                                billingId: updateBill.id,
                                amount: updateBill.finalAmount,
                                lastPayment: lastPayment,
                                currentPlanId: updateBill.planId,
                                successOrFail: successOrFail
                            }
                        });
                    }

                    return updateBill;

                } else {
                    throw new Error('Something want wrong');
                }

            }
            throw new Error('Already used by company');
        } else {

            const billingSummaryRes = await context.prisma.createBillingSummary({
                planId: args.planId,
                whatsIncluded: args.whatsIncluded,
                companyId: args.companyId,
                paymentMethodId: args.paymentMethodId,
                billingContact: args.billingContact,
                directPay: args.directPay,
                userId: args.userId,
                user: {
                    connect: {
                        id: args.userId
                    }
                }

            });
            if (billingSummaryRes) {
                const responsePlan = await context.prisma.plan({
                    id: billingSummaryRes.planId
                });

                const updateBill = await context.prisma.updateBillingSummary({
                    where: {
                        id: billingSummaryRes.id
                    },
                    data: {
                        finalAmount: responsePlan.amount,
                        directPay: args.directPay
                    }
                });

                if (updateBill) {
                    const lastPayment = new Date();
                    let successOrFail;
                    switch (updateBill.directPay) {
                        case 'on':
                            successOrFail = 'true';
                            break;
                        case 'off':
                            successOrFail = 'false';
                            break;
                        default:
                            successOrFail = 'false';
                    }

                    await context.prisma.updateCompany({
                        where: {
                            id: updateBill.companyId
                        },
                        data: {
                            billingId: updateBill.id,
                            amount: updateBill.finalAmount,
                            lastPayment: lastPayment,
                            currentPlanId: updateBill.planId,
                            successOrFail: successOrFail
                        }
                    });


                }
                return updateBill;
            }
        }
    },

    deleteBillingSummary: async (parent, {
        id
    }, context) => {
        return await context.prisma.deleteBillingSummary({
            id
        });
    },

    applyPromoCode: async (parent, args, context) => {

        const responsePromoCode = await context.prisma.promoCodes({
            "where": {
                "promoCode": args.promoCode
            }
        });


        const foundPromoCode = await context.prisma.billingSummaries({
            where: {
                AND: [{
                        companyId: args.companyId
                    },
                    {
                        promoCodeId: responsePromoCode[0].id
                    }
                ]
            }
        });

        if (foundPromoCode.length === 0) {
            return await context.prisma.updatePromoCode({
                where: {
                    id: responsePromoCode[0].id
                },
                data: {
                    isVaild: "true"
                }
            });
        } else {

            return await context.prisma.updatePromoCode({
                where: {
                    id: responsePromoCode[0].id
                },
                data: {
                    isVaild: "false"
                }
            });
        }
    },
    createAddCard: async (parent, args, context) => {
         return await context.prisma.createAddCard({
            cardNumber: args.cardNumber,
            cardType: args.cardType,
            companyId: args.companyId,
            expirationDate: args.expirationDate,
            securityCode: args.securityCode,
            nameOnCard: args.nameOnCard,
            addressLine1: args.addressLine1,
            addressLine2: args.addressLine2,
            city: args.city,
            state: args.state,
            zipCode: args.zipCode,
            userId: args.userId,
            user: {
                connect: {
                    id: args.userId
                }
            }

        });
    },

    updateAddCard: async (parent, args, context) => {
        return await context.prisma.updateAddCard({
            where: {
                id: args.addCardId
            },
            data: {
                cardNumber: args.cardNumber,
                cardType: args.cardType,
                companyId: args.companyId,
                expirationDate: args.expirationDate,
                securityCode: args.securityCode,
                nameOnCard: args.nameOnCard,
                addressLine1: args.addressLine1,
                addressLine2: args.addressLine2,
                city: args.city,
                state: args.state,
                zipCode: args.zipCode,
                userId: args.userId,
                user: {
                    connect: {
                        id: args.userId
                    }
                }
            }

        });
    },



    deletePaymentMethod: async (parent, args, context) => {
        return await context.prisma.updatePaymentMethod({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },


    // plan
    createPlan: async (parent, args, context) => {
        return await context.prisma.createPlan({
            planName: args.planName,
            currency: args.currency,
            amount: args.amount,
            maxRetro: args.maxRetro,
            maxRetroAdmin: args.maxRetroAdmin,
            maxCompanyAdmin: args.maxCompanyAdmin

        });
    },

    updatePlan: async (parent, args, context) => {
        return await context.prisma.updatePlan({
            where: {
                id: args.planId
            },
            data: {

                planName: args.planName,
                currency: args.currency,
                amount: args.amount,
                maxRetro: args.maxRetro,
                maxRetroAdmin: args.maxRetroAdmin,
                maxCompanyAdmin: args.maxCompanyAdmin
            }

        });
    },

    deletePlan: async (parent, args, context) => {
        return await context.prisma.updatePlan({
            where: {
                id: args.id
            },
            data: {
                isDeleted: "true"

            }

        });
    },

    //PromoCode

    createPromoCode: async (parent, args, context) => {
        // console.log(args)
        return await context.prisma.createPromoCode({
            promoCode: args.promoCode,
            target: args.target,
            discountValue: args.discountValue,
            limitValue: args.limitValue,
            repeat: args.repeat,
            startDate: args.startDate,
            endDate: args.endDate,
            usedBy: args.usedBy,
            limit: args.limit,
            unit: args.unit,
            userId: args.userId,

        });
    },


    // console.log(args)
    updatePromoCode: async (parent, args, context) => {
        return await context.prisma.updatePromoCode({
            where: {
                id: args.PromoCodeId
            },
            data: {
                promoCode: args.promoCode,
                target: args.target,
                discountValue: args.discountValue,
                limitValue: args.limitValue,
                repeat: args.repeat,
                startDate: args.startDate,
                isDeleted: args.isDeleted,
                endDate: args.endDate,
                usedBy: args.usedBy,
                limit: args.limit,
                unit: args.unit,

            }

        });
    },




    deletePromoCode: async (parent, args, context) => {
        return await context.prisma.deletePromoCode({
            id: args.id
        })

    },


};

module.exports = {
    Mutation,
};