const PromoCode = {
    user: ({ id }, args, context) => {
        return context.prisma.promoCode({ id }).user()
    },
    
}

module.exports = {
    PromoCode,
}
