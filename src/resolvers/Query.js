const nodemailer = require('nodemailer');
const {
    userInviteForRetroJoin
} = require('../utils/retroInvite');

const Query = {

    fetchRetroByRoomCode: async (parent, args, context) => {
        const allRetro = await context.prisma.retroes({
            "where": {
                "roomCode": args.roomCode
            }
        });
        return allRetro
    },


    // Auth User
    fetchSingleUser: async (parent, args, context) => {
        return await context.prisma.user({
            id: args.id
        });
    },

    fetchAllUser: async (parent, args, context) => {
        var User = await context.prisma.users();
        var userArray = [];
        for (var i = 0; i < User.length; i++) {
            var user_id = User[i].id;
            var retroData = await context.prisma.retroes({
                "where": {
                    "userid": {
                        "id": user_id
                    }
                }
            });
            User[i].retroCount = retroData.length;
        }

        return User
    },

fetchUserByCompanyId: async (root, args, context) => {

    if (args.isGuest === 'true' || args.hideArchived === 'true') {

        if (args.isGuest === 'true') {


            return await context.prisma.users({
                where: {
                    AND: [{
                            companyId: args.companyId
                        },
                        {
                            isDeleted: "false"
                        },
                        {
                            isGuest: "false"
                        }
                    ],
                }

            });

        }

        if (args.hideArchived === 'true') {


            return await context.prisma.users({
                where: {
                    AND: [{
                            companyId: args.companyId
                        },
                        {
                            isDeleted: "false"
                        },
                        {
                            hideArchived: "false"
                        }
                    ],
                }

            });

        }

    } else {


        return await context.prisma.users({
            where: {
                AND: [{
                        companyId: args.companyId
                    },
                    {
                        isDeleted: "false"
                    }
                ],
            }

        });
    }
},
    // Default User Project
    getallproject: async (parent, args, context) => {
        var defaultProject = await context.prisma.defaultProjects();

        var projectArray = [];
        for (var i = 0; i < defaultProject.length; i++) {
            var project_id = defaultProject[i].id;
            var retroData = await context.prisma.retroes({
                "where": {
                    "projectId": project_id
                }
            });
            defaultProject[i].retroCount = retroData.length;
        }

        return defaultProject
    },

    fetchSingleProject: async (parent, {
        id
    }, context) => {
        const defaultProject = await context.prisma.defaultProject({
            id
        });
        return defaultProject
    },

    getAllProjectByUser: async (parent, args, context) => {
        const userProjectData = await context.prisma.defaultProjects({
            where: {
                createdBy: {
                    id: args.userid
                }
            }
        });
        return userProjectData;
    },

    // Default Template
    specifictemplate: async (parent, args, context) => {
        const defaultTemplate = await context.prisma.defaultTemplates();

        let foundNotDeletedTemp = [];

        for (var i = 0; i < defaultTemplate.length; i++) {
            var template_id = defaultTemplate[i].id;
            var retroData = await context.prisma.retroes({
                "where": {
                    "templateId": template_id
                }
            });
            defaultTemplate[i].retroCount = retroData.length;
        }

        defaultTemplate.map((t) => {
            if (t.isDeleted !== null && t.isDeleted !== 'true') {

                foundNotDeletedTemp.push(t);
            }
        });

        return foundNotDeletedTemp;
    },

    singleDefaultTemplate: async (parent, {
        id
    }, context) => {
        const defaultTemplate = await context.prisma.defaultTemplate({
            id
        });
        return defaultTemplate;
    },


    // User Template
    fetchUsertemplate: async (parent, args, context) => {
        const userTemplateData = await context.prisma.userTemplates({
            where: {
                AND: [{
                    userid: {
                        id: args.userid
                    }
                }, {
                    isDeleted: "false"
                }]
            }
        });

        return userTemplateData;
    },
    fetchSingleUsertemplate: async (parent, args, context) => {
        const userTemplateData = await context.prisma.userTemplate({
            id: args.id
        });
        return userTemplateData;
    },

    fetchAllUserTemplate: async (parent, args, context, info) => {
        const userTemplateData = await context.prisma.userTemplates({
            isDeleted: "false"
        });
        return userTemplateData
    },

    // Retro
    sendRetroInvitedUser: async (parent, args, context) => {

        const foundRetro = await context.prisma.retro({
            id: args.retroId
        });

        let projectName, sprint, roomCode, startData, repeatEvery, companyName, firstName;

        if (foundRetro) {
            const userInfo = await context.prisma.user({
                id: args.userId
            });

            if (userInfo) {
                const companyData = await context.prisma.company({
                    id: userInfo.companyId
                });
                companyName = companyData.companyName;
            }

            roomCode = foundRetro.roomCode;
            sprint = foundRetro.sprintnumber;
            startData = foundRetro.startData ? foundRetro.startData : new Date();
            repeatEvery = foundRetro.repeatEvery ? foundRetro.repeatEvery : ' ';

            firstName = userInfo.firstName ? userInfo.firstName : userInfo.email;

            const projectData = await context.prisma.defaultProject({
                id: foundRetro.projectId
            });
            projectName = projectData.projectname;

            const fetchInvite = await context.prisma.inviteToJointRetroes({
                where: {
                    retroId: args.retroId
                }
            });

            if (fetchInvite) {

                fetchInvite.map((i) => {
                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'lalit.emaster@gmail.com',
                            pass: 'Lalit001'
                        }
                    });

                    const mailOptions = userInviteForRetroJoin(i.email, sprint, projectName, startData, roomCode, repeatEvery, firstName, companyName);

                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                });
            }

        }
        return foundRetro;
    },


    fetchSingleRetro: async (parent, args, context) => {
        const responseData = await context.prisma.retro({
            id: args.id
        });
        return responseData;
    },

    fetchAllRetro: async (parent, args, context, info) => {
        const responseRetroList = await context.prisma.retroes({
            where: {
                isDeleted: "false"
            }
        });

        return responseRetroList
    },

    fetchSingleRetroByUserId: async (parent, args, context, info) => {
        const responseRetro = await context.prisma.retroes({
            where: {
                userid: {
                    id: args.userid
                }
            }
        });
        return responseRetro;
    },

    // Invite Link
    fetchAllInviteLink: async (parent, args, context, info) => {
        const responseInvite = await context.prisma.inviteToJointRetroes({
            where: {
                retroId: args.retroid
            }
        });
        return responseInvite;
    },

    fetchAndMatchInviteLink: async (parent, args, context, info) => {

        if (args.isMyRetro == "true" || args.isUpcoming == "true" || args.isHistory == "true") {

            var whereArray = [];
            var todayDate = new Date();
            if (args.isMyRetro == "true") {
                whereArray.push({
                    retro: {
                        userid: {
                            id: args.userId
                        }
                    }
                });
            }
            if (args.isUpcoming == "true") {
                whereArray.push({
                    retro: {
                        startDate_gte: todayDate
                    }
                });
            }
            if (args.isHistory == "true") {
                whereArray.push({
                    retro: {
                        startDate_lt: todayDate
                    }
                });
            }

            const user = await context.prisma.user({
                id: args.userId
            })
            if (!user) {
                throw new Error(`No such user invited`)
            }
            whereArray.push({
                email: user.email
            });

            const val = await context.prisma.inviteToJointRetroes({
                where: {
                    AND: whereArray
                }
            });
            return val;


        } else {
            const user = await context.prisma.user({
                id: args.userId
            })
            if (!user) {
                throw new Error(`No such user invited`)
            }
            const val = await context.prisma.inviteToJointRetroes({
                where: {
                    AND: [{
                        email: user.email
                    }, {
                        retro: {
                            isDeleted: "false"
                        }
                    }]
                }
            });
            return val;
        }
    },

    //Thought
    fetchThoughts: async (parent, args, context) => {
        const responseThoughtList = await context.prisma.thoughts()
        return responseThoughtList;

    },

    fetchSingleThought: async (parent, {
        id
    }, context) => {
        const responseSingleThought = await context.prisma.thought({
            id
        })
        return responseSingleThought;

    },

    //Company

    fetchAllCompanies: async (parent, args, context) => {
        const responseCompanyList = await context.prisma.companies()
        return responseCompanyList;
    },

    fetchSingleCompany: async (parent, {
        id
    }, context) => {
        const responseCompanyList = await context.prisma.company({
            id
        })
        return responseCompanyList;
    },

    // PaymentMethod
    fetchAllAddCard: async (parent, args, context) => {
        const responseAddCard = await context.prisma.addCards()
        return responseAddCard;
    },

    fetchAddCardByCompanyId: async (parent, args, context) => {
        const responseAddCard = await context.prisma.addCards({
            where: {
                companyId: args.companyId
            }
        })
        return responseAddCard;
    },

    // PaymentMethod
    fetchSingleAddCard: async (parent, {
        id
    }, context) => {
        const responseAddCard = await context.prisma.addCard({
            id
        })
        return responseAddCard;
    },

    // Plan
    fetchAllPlans: async (parent, args, context) => {
        const responsePlan = await context.prisma.plans()
        return responsePlan;
    },

    fetchSinglePlan: async (parent, {
        id
    }, context) => {
        const responsePlan = await context.prisma.plan({
            id
        })
        return responsePlan;
    },

    // Plan
    fetchAllPromoCodes: async (parent, args, context) => {
        const responsePromoCode = await context.prisma.promoCodes()
        return responsePromoCode;
    },

    fetchSinglePromoCode: async (parent, {
        id
    }, context) => {
        const responsePromoCode = await context.prisma.promoCode({
            id
        });
        return responsePromoCode;
    },



    // Billing Summary
    fetchAllBillingSummary: async (parent, args, context) => {
        const responseBillingSummary = await context.prisma.billingSummaries();
        return responseBillingSummary;
    },

    fetchAllVoiceHistory: async (parent, args, context) => {
        const responseBillingSummary = await context.prisma.billingSummaries({
            where: {
                companyId: args.companyId
            }
        });

        if (responseBillingSummary.length !== 0) {

            return responseBillingSummary.map(async (b, i) => {
                const planInfo = await context.prisma.plan({
                    id: b.planId
                });

                b.planInfo = JSON.stringify(planInfo);
                const paymentInfo = await context.prisma.addCard({
                    id: b.paymentMethodId
                });
                b.paymentInfo = JSON.stringify(paymentInfo);

                return b;
            });
            // return billInfo;

        }
        return responseBillingSummary;

    },

    retroAdminInfo: async (parent, args, context) => {
        const companyData = await context.prisma.company({
            id: args.companyId
        });

        if (companyData) {
            const fetchPlan = await context.prisma.plan({
                id: companyData.currentPlanId
            });
            companyData.planInfo = JSON.stringify(fetchPlan);

            const retroAdmin = await context.prisma.users({
                where: {
                    AND: [{
                            companyId: companyData.id
                        },
                        {
                            isRetroAdmin: 'true'
                        }
                    ]
                }
            });

            companyData.retroAdminCount = retroAdmin.length;
        }
        return companyData;
    },


    adminUserBar: async (parent, args, context) => {
        const companyData = await context.prisma.company({
            id: args.companyId
        });

        if (companyData) {
            const fetchPlan = await context.prisma.plan({
                id: companyData.currentPlanId
            });
            companyData.planInfo = JSON.stringify(fetchPlan);

            const retroAdmin = await context.prisma.users({
                where: {
                    AND: [{
                            companyId: companyData.id
                        },
                        {
                            isRetroAdmin: 'true'
                        }
                    ]
                }
            });
            companyData.retroAdminCount = retroAdmin.length;

            const companyAdmin = await context.prisma.users({
                where: {
                    AND: [{
                            companyId: companyData.id
                        },
                        {
                            isCompanyAdmin: 'true'
                        }
                    ]
                }
            });
            companyData.companyAdminCount = companyAdmin.length;

            const companyGuest = await context.prisma.users({
                where: {
                    AND: [{
                            companyId: companyData.id
                        },
                        {
                            isGuest: 'true'
                        }
                    ]
                }
            });
            companyData.guestCount = companyGuest.length;

            const RegisteredTotal = await context.prisma.users({
                where: {
                    companyId: companyData.id
                }
            });
            companyData.registeredAttendesCount = RegisteredTotal.length;
        }

        return companyData;
    },
}

module.exports = {
    Query,
}