const { Query } = require('./Query');
const { Mutation } = require('./Mutation');
const { User } = require('./User');
const { userTemplate } = require('./userTemplate');
const { Thought } = require('./Thought');
const { defaultProject } = require('./defaultProject');
const { InviteToJointRetro } = require('./InviteLink');
const { BillingSummary } = require('./BillingSummary');
const { AddCard } = require('./AddCard');
const { PromoCode } = require('./PromoCode');
const { Company } = require('./Company');

const Retro = {
    userid: ({ id }, args, context) => {
        return context.prisma.retro({ id }).userid();
    },
    inviteToJointRetros: (parrent, args, context, info) => {
        return context.prisma.retro({ id: parrent.id }).inviteToJointRetros();
    },
    thoughts:({ id }, args, context) => {
        return context.prisma.retro({ id }).thoughts();
    }, 

    projects:({ id }, args, context) => {
        return context.prisma.retro({ id }).projects();
    }, 
};

const ThoughtGroup = {
      thoughts:({ id }, args, context) => {
        return context.prisma.thoughtGroup({ id }).thoughts();
    }, 

}
const resolvers = {
    Query,
    Mutation,
    User,
    userTemplate,
    Retro,
    Thought,
    defaultProject,
    InviteToJointRetro,
    BillingSummary,
    AddCard,
    Company,

};

module.exports = {
  resolvers,
};
