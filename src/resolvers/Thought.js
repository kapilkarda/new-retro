const Thought = {
    userid: ({ id }, args, context) => {
        return context.prisma.thought({ id }).userid()
    },
    retroid: ({ id }, args, context) => {
        return context.prisma.thought({ id }).retroid()
    },
}

module.exports = {
    Thought,
}
