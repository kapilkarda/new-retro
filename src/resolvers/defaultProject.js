const defaultProject = {
    createdBy: ({ id }, args, context) => {
        return context.prisma.defaultProject({ id }).createdBy()
    },
}

module.exports = {
    defaultProject,
}
