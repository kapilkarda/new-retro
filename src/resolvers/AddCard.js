const AddCard = {
    user: ({ id }, args, context) => {
        return context.prisma.addCard({ id }).user()
    },
    
}

module.exports = {
    AddCard,
}
