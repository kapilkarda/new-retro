const User = {
   userTemplates: ({id}, args, context) => {
        return context.prisma.user({id}).userTemplates()
    },
    retros: ({id}, args, context) => {
        return context.prisma.user({id}).retros()
    },
    thoughts: ({id}, args, context) => {
        return context.prisma.user({id}).thoughts()
    },
    projects: ({id}, args, context) => {
        return context.prisma.user({id}).projects()
    },
    projects: ({id}, args, context) => {
        return context.prisma.user({id}).projects()
    },
    company: ({id}, args, context) => {
        return context.prisma.user({id}).company()
    },

    promoCode: ({id}, args, context) => {
       return context.prisma.user({id}).promoCode()
    },
  }

module.exports = {
    User,
}
