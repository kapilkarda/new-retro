const userTemplate = {
    userid: ({ id }, args, context) => {
        return context.prisma.userTemplate({ id }).userid()
    },
      retro: ({ id }, args, context) => {
        return context.prisma.userTemplate({ id }).retro()
    },
}

module.exports = {
    userTemplate,
}
