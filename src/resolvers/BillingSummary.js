const BillingSummary = {
    user: ({ id }, args, context) => {
        return context.prisma.billingSummary({ id }).user()
    },
    
}

module.exports = {
    BillingSummary,
}
