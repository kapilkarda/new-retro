const defaultTemplate = {
    userid: ({ id }, args, context) => {
        return context.prisma.defaultTemplate({ id }).userid()
    },
    retro: ({ id }, args, context) => {
        return context.prisma.defaultTemplate({ id }).retro()
    },
}

module.exports = {
    defaultTemplate,
}