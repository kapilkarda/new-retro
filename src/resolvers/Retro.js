const Retro = {
    userid: ({ id }, args, context) => {
        return context.prisma.retro({ id }).userid()
    },
    inviteToJointRetros: (parrent, args, context, info) => {
        return context.prisma.retro({ id: parrent.id }).inviteToJointRetros()
    },
    thoughts:({ id }, args, context) => {
        return context.prisma.retro({ id }).thoughts()
    }, 

    projects:({ id }, args, context) => {
        return context.prisma.retro({ id }).projects()
    }, 

    userTemplate:({ id }, args, context) => {
        return context.prisma.retro({ id }).userTemplate()
    }, 

}

module.exports = {
    Retro,
}
