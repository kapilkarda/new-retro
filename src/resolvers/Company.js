const Company = {
    user: ({ id }, args, context) => {
        return context.prisma.company({ id }).user();
    },
};

module.exports = {
    Company,
};
