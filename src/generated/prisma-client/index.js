"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "AddCard",
    embedded: false
  },
  {
    name: "BillingSummary",
    embedded: false
  },
  {
    name: "Company",
    embedded: false
  },
  {
    name: "InviteToJointRetro",
    embedded: false
  },
  {
    name: "Plan",
    embedded: false
  },
  {
    name: "PromoCode",
    embedded: false
  },
  {
    name: "Retro",
    embedded: false
  },
  {
    name: "Thought",
    embedded: false
  },
  {
    name: "User",
    embedded: false
  },
  {
    name: "defaultProject",
    embedded: false
  },
  {
    name: "defaultTemplate",
    embedded: false
  },
  {
    name: "userTemplate",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `http://45.61.48.46:4466`
});
exports.prisma = new exports.Prisma();
