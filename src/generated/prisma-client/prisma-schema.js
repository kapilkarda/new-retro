module.exports = {
        typeDefs: /* GraphQL */ `type AddCard {
  id: ID!
  companyId: String
  userId: String
  user: User!
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type AddCardConnection {
  pageInfo: PageInfo!
  edges: [AddCardEdge]!
  aggregate: AggregateAddCard!
}

input AddCardCreateInput {
  companyId: String
  userId: String
  user: UserCreateOneWithoutAddCardInput!
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardCreateManyWithoutUserInput {
  create: [AddCardCreateWithoutUserInput!]
  connect: [AddCardWhereUniqueInput!]
}

input AddCardCreateOneInput {
  create: AddCardCreateInput
  connect: AddCardWhereUniqueInput
}

input AddCardCreateWithoutUserInput {
  companyId: String
  userId: String
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

type AddCardEdge {
  node: AddCard!
  cursor: String!
}

enum AddCardOrderByInput {
  id_ASC
  id_DESC
  companyId_ASC
  companyId_DESC
  userId_ASC
  userId_DESC
  cardNumber_ASC
  cardNumber_DESC
  cardType_ASC
  cardType_DESC
  expirationDate_ASC
  expirationDate_DESC
  securityCode_ASC
  securityCode_DESC
  nameOnCard_ASC
  nameOnCard_DESC
  addressLine1_ASC
  addressLine1_DESC
  addressLine2_ASC
  addressLine2_DESC
  city_ASC
  city_DESC
  state_ASC
  state_DESC
  isDefault_ASC
  isDefault_DESC
  zipCode_ASC
  zipCode_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type AddCardPreviousValues {
  id: ID!
  companyId: String
  userId: String
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input AddCardScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  companyId: String
  companyId_not: String
  companyId_in: [String!]
  companyId_not_in: [String!]
  companyId_lt: String
  companyId_lte: String
  companyId_gt: String
  companyId_gte: String
  companyId_contains: String
  companyId_not_contains: String
  companyId_starts_with: String
  companyId_not_starts_with: String
  companyId_ends_with: String
  companyId_not_ends_with: String
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  cardNumber: String
  cardNumber_not: String
  cardNumber_in: [String!]
  cardNumber_not_in: [String!]
  cardNumber_lt: String
  cardNumber_lte: String
  cardNumber_gt: String
  cardNumber_gte: String
  cardNumber_contains: String
  cardNumber_not_contains: String
  cardNumber_starts_with: String
  cardNumber_not_starts_with: String
  cardNumber_ends_with: String
  cardNumber_not_ends_with: String
  cardType: String
  cardType_not: String
  cardType_in: [String!]
  cardType_not_in: [String!]
  cardType_lt: String
  cardType_lte: String
  cardType_gt: String
  cardType_gte: String
  cardType_contains: String
  cardType_not_contains: String
  cardType_starts_with: String
  cardType_not_starts_with: String
  cardType_ends_with: String
  cardType_not_ends_with: String
  expirationDate: String
  expirationDate_not: String
  expirationDate_in: [String!]
  expirationDate_not_in: [String!]
  expirationDate_lt: String
  expirationDate_lte: String
  expirationDate_gt: String
  expirationDate_gte: String
  expirationDate_contains: String
  expirationDate_not_contains: String
  expirationDate_starts_with: String
  expirationDate_not_starts_with: String
  expirationDate_ends_with: String
  expirationDate_not_ends_with: String
  securityCode: String
  securityCode_not: String
  securityCode_in: [String!]
  securityCode_not_in: [String!]
  securityCode_lt: String
  securityCode_lte: String
  securityCode_gt: String
  securityCode_gte: String
  securityCode_contains: String
  securityCode_not_contains: String
  securityCode_starts_with: String
  securityCode_not_starts_with: String
  securityCode_ends_with: String
  securityCode_not_ends_with: String
  nameOnCard: String
  nameOnCard_not: String
  nameOnCard_in: [String!]
  nameOnCard_not_in: [String!]
  nameOnCard_lt: String
  nameOnCard_lte: String
  nameOnCard_gt: String
  nameOnCard_gte: String
  nameOnCard_contains: String
  nameOnCard_not_contains: String
  nameOnCard_starts_with: String
  nameOnCard_not_starts_with: String
  nameOnCard_ends_with: String
  nameOnCard_not_ends_with: String
  addressLine1: String
  addressLine1_not: String
  addressLine1_in: [String!]
  addressLine1_not_in: [String!]
  addressLine1_lt: String
  addressLine1_lte: String
  addressLine1_gt: String
  addressLine1_gte: String
  addressLine1_contains: String
  addressLine1_not_contains: String
  addressLine1_starts_with: String
  addressLine1_not_starts_with: String
  addressLine1_ends_with: String
  addressLine1_not_ends_with: String
  addressLine2: String
  addressLine2_not: String
  addressLine2_in: [String!]
  addressLine2_not_in: [String!]
  addressLine2_lt: String
  addressLine2_lte: String
  addressLine2_gt: String
  addressLine2_gte: String
  addressLine2_contains: String
  addressLine2_not_contains: String
  addressLine2_starts_with: String
  addressLine2_not_starts_with: String
  addressLine2_ends_with: String
  addressLine2_not_ends_with: String
  city: String
  city_not: String
  city_in: [String!]
  city_not_in: [String!]
  city_lt: String
  city_lte: String
  city_gt: String
  city_gte: String
  city_contains: String
  city_not_contains: String
  city_starts_with: String
  city_not_starts_with: String
  city_ends_with: String
  city_not_ends_with: String
  state: String
  state_not: String
  state_in: [String!]
  state_not_in: [String!]
  state_lt: String
  state_lte: String
  state_gt: String
  state_gte: String
  state_contains: String
  state_not_contains: String
  state_starts_with: String
  state_not_starts_with: String
  state_ends_with: String
  state_not_ends_with: String
  isDefault: Boolean
  isDefault_not: Boolean
  zipCode: String
  zipCode_not: String
  zipCode_in: [String!]
  zipCode_not_in: [String!]
  zipCode_lt: String
  zipCode_lte: String
  zipCode_gt: String
  zipCode_gte: String
  zipCode_contains: String
  zipCode_not_contains: String
  zipCode_starts_with: String
  zipCode_not_starts_with: String
  zipCode_ends_with: String
  zipCode_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [AddCardScalarWhereInput!]
  OR: [AddCardScalarWhereInput!]
  NOT: [AddCardScalarWhereInput!]
}

type AddCardSubscriptionPayload {
  mutation: MutationType!
  node: AddCard
  updatedFields: [String!]
  previousValues: AddCardPreviousValues
}

input AddCardSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: AddCardWhereInput
  AND: [AddCardSubscriptionWhereInput!]
  OR: [AddCardSubscriptionWhereInput!]
  NOT: [AddCardSubscriptionWhereInput!]
}

input AddCardUpdateDataInput {
  companyId: String
  userId: String
  user: UserUpdateOneRequiredWithoutAddCardInput
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardUpdateInput {
  companyId: String
  userId: String
  user: UserUpdateOneRequiredWithoutAddCardInput
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardUpdateManyDataInput {
  companyId: String
  userId: String
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardUpdateManyMutationInput {
  companyId: String
  userId: String
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardUpdateManyWithoutUserInput {
  create: [AddCardCreateWithoutUserInput!]
  delete: [AddCardWhereUniqueInput!]
  connect: [AddCardWhereUniqueInput!]
  disconnect: [AddCardWhereUniqueInput!]
  update: [AddCardUpdateWithWhereUniqueWithoutUserInput!]
  upsert: [AddCardUpsertWithWhereUniqueWithoutUserInput!]
  deleteMany: [AddCardScalarWhereInput!]
  updateMany: [AddCardUpdateManyWithWhereNestedInput!]
}

input AddCardUpdateManyWithWhereNestedInput {
  where: AddCardScalarWhereInput!
  data: AddCardUpdateManyDataInput!
}

input AddCardUpdateOneInput {
  create: AddCardCreateInput
  update: AddCardUpdateDataInput
  upsert: AddCardUpsertNestedInput
  delete: Boolean
  disconnect: Boolean
  connect: AddCardWhereUniqueInput
}

input AddCardUpdateWithoutUserDataInput {
  companyId: String
  userId: String
  cardNumber: String
  cardType: String
  expirationDate: String
  securityCode: String
  nameOnCard: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  isDefault: Boolean
  zipCode: String
  isDeleted: String
}

input AddCardUpdateWithWhereUniqueWithoutUserInput {
  where: AddCardWhereUniqueInput!
  data: AddCardUpdateWithoutUserDataInput!
}

input AddCardUpsertNestedInput {
  update: AddCardUpdateDataInput!
  create: AddCardCreateInput!
}

input AddCardUpsertWithWhereUniqueWithoutUserInput {
  where: AddCardWhereUniqueInput!
  update: AddCardUpdateWithoutUserDataInput!
  create: AddCardCreateWithoutUserInput!
}

input AddCardWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  companyId: String
  companyId_not: String
  companyId_in: [String!]
  companyId_not_in: [String!]
  companyId_lt: String
  companyId_lte: String
  companyId_gt: String
  companyId_gte: String
  companyId_contains: String
  companyId_not_contains: String
  companyId_starts_with: String
  companyId_not_starts_with: String
  companyId_ends_with: String
  companyId_not_ends_with: String
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  user: UserWhereInput
  cardNumber: String
  cardNumber_not: String
  cardNumber_in: [String!]
  cardNumber_not_in: [String!]
  cardNumber_lt: String
  cardNumber_lte: String
  cardNumber_gt: String
  cardNumber_gte: String
  cardNumber_contains: String
  cardNumber_not_contains: String
  cardNumber_starts_with: String
  cardNumber_not_starts_with: String
  cardNumber_ends_with: String
  cardNumber_not_ends_with: String
  cardType: String
  cardType_not: String
  cardType_in: [String!]
  cardType_not_in: [String!]
  cardType_lt: String
  cardType_lte: String
  cardType_gt: String
  cardType_gte: String
  cardType_contains: String
  cardType_not_contains: String
  cardType_starts_with: String
  cardType_not_starts_with: String
  cardType_ends_with: String
  cardType_not_ends_with: String
  expirationDate: String
  expirationDate_not: String
  expirationDate_in: [String!]
  expirationDate_not_in: [String!]
  expirationDate_lt: String
  expirationDate_lte: String
  expirationDate_gt: String
  expirationDate_gte: String
  expirationDate_contains: String
  expirationDate_not_contains: String
  expirationDate_starts_with: String
  expirationDate_not_starts_with: String
  expirationDate_ends_with: String
  expirationDate_not_ends_with: String
  securityCode: String
  securityCode_not: String
  securityCode_in: [String!]
  securityCode_not_in: [String!]
  securityCode_lt: String
  securityCode_lte: String
  securityCode_gt: String
  securityCode_gte: String
  securityCode_contains: String
  securityCode_not_contains: String
  securityCode_starts_with: String
  securityCode_not_starts_with: String
  securityCode_ends_with: String
  securityCode_not_ends_with: String
  nameOnCard: String
  nameOnCard_not: String
  nameOnCard_in: [String!]
  nameOnCard_not_in: [String!]
  nameOnCard_lt: String
  nameOnCard_lte: String
  nameOnCard_gt: String
  nameOnCard_gte: String
  nameOnCard_contains: String
  nameOnCard_not_contains: String
  nameOnCard_starts_with: String
  nameOnCard_not_starts_with: String
  nameOnCard_ends_with: String
  nameOnCard_not_ends_with: String
  addressLine1: String
  addressLine1_not: String
  addressLine1_in: [String!]
  addressLine1_not_in: [String!]
  addressLine1_lt: String
  addressLine1_lte: String
  addressLine1_gt: String
  addressLine1_gte: String
  addressLine1_contains: String
  addressLine1_not_contains: String
  addressLine1_starts_with: String
  addressLine1_not_starts_with: String
  addressLine1_ends_with: String
  addressLine1_not_ends_with: String
  addressLine2: String
  addressLine2_not: String
  addressLine2_in: [String!]
  addressLine2_not_in: [String!]
  addressLine2_lt: String
  addressLine2_lte: String
  addressLine2_gt: String
  addressLine2_gte: String
  addressLine2_contains: String
  addressLine2_not_contains: String
  addressLine2_starts_with: String
  addressLine2_not_starts_with: String
  addressLine2_ends_with: String
  addressLine2_not_ends_with: String
  city: String
  city_not: String
  city_in: [String!]
  city_not_in: [String!]
  city_lt: String
  city_lte: String
  city_gt: String
  city_gte: String
  city_contains: String
  city_not_contains: String
  city_starts_with: String
  city_not_starts_with: String
  city_ends_with: String
  city_not_ends_with: String
  state: String
  state_not: String
  state_in: [String!]
  state_not_in: [String!]
  state_lt: String
  state_lte: String
  state_gt: String
  state_gte: String
  state_contains: String
  state_not_contains: String
  state_starts_with: String
  state_not_starts_with: String
  state_ends_with: String
  state_not_ends_with: String
  isDefault: Boolean
  isDefault_not: Boolean
  zipCode: String
  zipCode_not: String
  zipCode_in: [String!]
  zipCode_not_in: [String!]
  zipCode_lt: String
  zipCode_lte: String
  zipCode_gt: String
  zipCode_gte: String
  zipCode_contains: String
  zipCode_not_contains: String
  zipCode_starts_with: String
  zipCode_not_starts_with: String
  zipCode_ends_with: String
  zipCode_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [AddCardWhereInput!]
  OR: [AddCardWhereInput!]
  NOT: [AddCardWhereInput!]
}

input AddCardWhereUniqueInput {
  id: ID
}

type AggregateAddCard {
  count: Int!
}

type AggregateBillingSummary {
  count: Int!
}

type AggregateCompany {
  count: Int!
}

type AggregatedefaultProject {
  count: Int!
}

type AggregatedefaultTemplate {
  count: Int!
}

type AggregateInviteToJointRetro {
  count: Int!
}

type AggregatePlan {
  count: Int!
}

type AggregatePromoCode {
  count: Int!
}

type AggregateRetro {
  count: Int!
}

type AggregateThought {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type AggregateuserTemplate {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type BillingSummary {
  id: ID!
  userId: String
  user: User!
  planId: String
  plan: Plan
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  promoCode: PromoCode
  paymentMethodId: String
  paymentMethod: AddCard
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type BillingSummaryConnection {
  pageInfo: PageInfo!
  edges: [BillingSummaryEdge]!
  aggregate: AggregateBillingSummary!
}

input BillingSummaryCreateInput {
  userId: String
  user: UserCreateOneWithoutBillingSummaryInput!
  planId: String
  plan: PlanCreateOneInput
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  promoCode: PromoCodeCreateOneInput
  paymentMethodId: String
  paymentMethod: AddCardCreateOneInput
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

input BillingSummaryCreateManyWithoutUserInput {
  create: [BillingSummaryCreateWithoutUserInput!]
  connect: [BillingSummaryWhereUniqueInput!]
}

input BillingSummaryCreateWithoutUserInput {
  userId: String
  planId: String
  plan: PlanCreateOneInput
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  promoCode: PromoCodeCreateOneInput
  paymentMethodId: String
  paymentMethod: AddCardCreateOneInput
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

type BillingSummaryEdge {
  node: BillingSummary!
  cursor: String!
}

enum BillingSummaryOrderByInput {
  id_ASC
  id_DESC
  userId_ASC
  userId_DESC
  planId_ASC
  planId_DESC
  companyId_ASC
  companyId_DESC
  whatsIncluded_ASC
  whatsIncluded_DESC
  promoCodeId_ASC
  promoCodeId_DESC
  paymentMethodId_ASC
  paymentMethodId_DESC
  billingContact_ASC
  billingContact_DESC
  finalAmount_ASC
  finalAmount_DESC
  directPay_ASC
  directPay_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type BillingSummaryPreviousValues {
  id: ID!
  userId: String
  planId: String
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  paymentMethodId: String
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input BillingSummaryScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  planId: String
  planId_not: String
  planId_in: [String!]
  planId_not_in: [String!]
  planId_lt: String
  planId_lte: String
  planId_gt: String
  planId_gte: String
  planId_contains: String
  planId_not_contains: String
  planId_starts_with: String
  planId_not_starts_with: String
  planId_ends_with: String
  planId_not_ends_with: String
  companyId: String
  companyId_not: String
  companyId_in: [String!]
  companyId_not_in: [String!]
  companyId_lt: String
  companyId_lte: String
  companyId_gt: String
  companyId_gte: String
  companyId_contains: String
  companyId_not_contains: String
  companyId_starts_with: String
  companyId_not_starts_with: String
  companyId_ends_with: String
  companyId_not_ends_with: String
  whatsIncluded: String
  whatsIncluded_not: String
  whatsIncluded_in: [String!]
  whatsIncluded_not_in: [String!]
  whatsIncluded_lt: String
  whatsIncluded_lte: String
  whatsIncluded_gt: String
  whatsIncluded_gte: String
  whatsIncluded_contains: String
  whatsIncluded_not_contains: String
  whatsIncluded_starts_with: String
  whatsIncluded_not_starts_with: String
  whatsIncluded_ends_with: String
  whatsIncluded_not_ends_with: String
  promoCodeId: String
  promoCodeId_not: String
  promoCodeId_in: [String!]
  promoCodeId_not_in: [String!]
  promoCodeId_lt: String
  promoCodeId_lte: String
  promoCodeId_gt: String
  promoCodeId_gte: String
  promoCodeId_contains: String
  promoCodeId_not_contains: String
  promoCodeId_starts_with: String
  promoCodeId_not_starts_with: String
  promoCodeId_ends_with: String
  promoCodeId_not_ends_with: String
  paymentMethodId: String
  paymentMethodId_not: String
  paymentMethodId_in: [String!]
  paymentMethodId_not_in: [String!]
  paymentMethodId_lt: String
  paymentMethodId_lte: String
  paymentMethodId_gt: String
  paymentMethodId_gte: String
  paymentMethodId_contains: String
  paymentMethodId_not_contains: String
  paymentMethodId_starts_with: String
  paymentMethodId_not_starts_with: String
  paymentMethodId_ends_with: String
  paymentMethodId_not_ends_with: String
  billingContact: String
  billingContact_not: String
  billingContact_in: [String!]
  billingContact_not_in: [String!]
  billingContact_lt: String
  billingContact_lte: String
  billingContact_gt: String
  billingContact_gte: String
  billingContact_contains: String
  billingContact_not_contains: String
  billingContact_starts_with: String
  billingContact_not_starts_with: String
  billingContact_ends_with: String
  billingContact_not_ends_with: String
  finalAmount: Float
  finalAmount_not: Float
  finalAmount_in: [Float!]
  finalAmount_not_in: [Float!]
  finalAmount_lt: Float
  finalAmount_lte: Float
  finalAmount_gt: Float
  finalAmount_gte: Float
  directPay: String
  directPay_not: String
  directPay_in: [String!]
  directPay_not_in: [String!]
  directPay_lt: String
  directPay_lte: String
  directPay_gt: String
  directPay_gte: String
  directPay_contains: String
  directPay_not_contains: String
  directPay_starts_with: String
  directPay_not_starts_with: String
  directPay_ends_with: String
  directPay_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [BillingSummaryScalarWhereInput!]
  OR: [BillingSummaryScalarWhereInput!]
  NOT: [BillingSummaryScalarWhereInput!]
}

type BillingSummarySubscriptionPayload {
  mutation: MutationType!
  node: BillingSummary
  updatedFields: [String!]
  previousValues: BillingSummaryPreviousValues
}

input BillingSummarySubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: BillingSummaryWhereInput
  AND: [BillingSummarySubscriptionWhereInput!]
  OR: [BillingSummarySubscriptionWhereInput!]
  NOT: [BillingSummarySubscriptionWhereInput!]
}

input BillingSummaryUpdateInput {
  userId: String
  user: UserUpdateOneRequiredWithoutBillingSummaryInput
  planId: String
  plan: PlanUpdateOneInput
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  promoCode: PromoCodeUpdateOneInput
  paymentMethodId: String
  paymentMethod: AddCardUpdateOneInput
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

input BillingSummaryUpdateManyDataInput {
  userId: String
  planId: String
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  paymentMethodId: String
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

input BillingSummaryUpdateManyMutationInput {
  userId: String
  planId: String
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  paymentMethodId: String
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

input BillingSummaryUpdateManyWithoutUserInput {
  create: [BillingSummaryCreateWithoutUserInput!]
  delete: [BillingSummaryWhereUniqueInput!]
  connect: [BillingSummaryWhereUniqueInput!]
  disconnect: [BillingSummaryWhereUniqueInput!]
  update: [BillingSummaryUpdateWithWhereUniqueWithoutUserInput!]
  upsert: [BillingSummaryUpsertWithWhereUniqueWithoutUserInput!]
  deleteMany: [BillingSummaryScalarWhereInput!]
  updateMany: [BillingSummaryUpdateManyWithWhereNestedInput!]
}

input BillingSummaryUpdateManyWithWhereNestedInput {
  where: BillingSummaryScalarWhereInput!
  data: BillingSummaryUpdateManyDataInput!
}

input BillingSummaryUpdateWithoutUserDataInput {
  userId: String
  planId: String
  plan: PlanUpdateOneInput
  companyId: String
  whatsIncluded: String
  promoCodeId: String
  promoCode: PromoCodeUpdateOneInput
  paymentMethodId: String
  paymentMethod: AddCardUpdateOneInput
  billingContact: String
  finalAmount: Float
  directPay: String
  isDeleted: String
}

input BillingSummaryUpdateWithWhereUniqueWithoutUserInput {
  where: BillingSummaryWhereUniqueInput!
  data: BillingSummaryUpdateWithoutUserDataInput!
}

input BillingSummaryUpsertWithWhereUniqueWithoutUserInput {
  where: BillingSummaryWhereUniqueInput!
  update: BillingSummaryUpdateWithoutUserDataInput!
  create: BillingSummaryCreateWithoutUserInput!
}

input BillingSummaryWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  user: UserWhereInput
  planId: String
  planId_not: String
  planId_in: [String!]
  planId_not_in: [String!]
  planId_lt: String
  planId_lte: String
  planId_gt: String
  planId_gte: String
  planId_contains: String
  planId_not_contains: String
  planId_starts_with: String
  planId_not_starts_with: String
  planId_ends_with: String
  planId_not_ends_with: String
  plan: PlanWhereInput
  companyId: String
  companyId_not: String
  companyId_in: [String!]
  companyId_not_in: [String!]
  companyId_lt: String
  companyId_lte: String
  companyId_gt: String
  companyId_gte: String
  companyId_contains: String
  companyId_not_contains: String
  companyId_starts_with: String
  companyId_not_starts_with: String
  companyId_ends_with: String
  companyId_not_ends_with: String
  whatsIncluded: String
  whatsIncluded_not: String
  whatsIncluded_in: [String!]
  whatsIncluded_not_in: [String!]
  whatsIncluded_lt: String
  whatsIncluded_lte: String
  whatsIncluded_gt: String
  whatsIncluded_gte: String
  whatsIncluded_contains: String
  whatsIncluded_not_contains: String
  whatsIncluded_starts_with: String
  whatsIncluded_not_starts_with: String
  whatsIncluded_ends_with: String
  whatsIncluded_not_ends_with: String
  promoCodeId: String
  promoCodeId_not: String
  promoCodeId_in: [String!]
  promoCodeId_not_in: [String!]
  promoCodeId_lt: String
  promoCodeId_lte: String
  promoCodeId_gt: String
  promoCodeId_gte: String
  promoCodeId_contains: String
  promoCodeId_not_contains: String
  promoCodeId_starts_with: String
  promoCodeId_not_starts_with: String
  promoCodeId_ends_with: String
  promoCodeId_not_ends_with: String
  promoCode: PromoCodeWhereInput
  paymentMethodId: String
  paymentMethodId_not: String
  paymentMethodId_in: [String!]
  paymentMethodId_not_in: [String!]
  paymentMethodId_lt: String
  paymentMethodId_lte: String
  paymentMethodId_gt: String
  paymentMethodId_gte: String
  paymentMethodId_contains: String
  paymentMethodId_not_contains: String
  paymentMethodId_starts_with: String
  paymentMethodId_not_starts_with: String
  paymentMethodId_ends_with: String
  paymentMethodId_not_ends_with: String
  paymentMethod: AddCardWhereInput
  billingContact: String
  billingContact_not: String
  billingContact_in: [String!]
  billingContact_not_in: [String!]
  billingContact_lt: String
  billingContact_lte: String
  billingContact_gt: String
  billingContact_gte: String
  billingContact_contains: String
  billingContact_not_contains: String
  billingContact_starts_with: String
  billingContact_not_starts_with: String
  billingContact_ends_with: String
  billingContact_not_ends_with: String
  finalAmount: Float
  finalAmount_not: Float
  finalAmount_in: [Float!]
  finalAmount_not_in: [Float!]
  finalAmount_lt: Float
  finalAmount_lte: Float
  finalAmount_gt: Float
  finalAmount_gte: Float
  directPay: String
  directPay_not: String
  directPay_in: [String!]
  directPay_not_in: [String!]
  directPay_lt: String
  directPay_lte: String
  directPay_gt: String
  directPay_gte: String
  directPay_contains: String
  directPay_not_contains: String
  directPay_starts_with: String
  directPay_not_starts_with: String
  directPay_ends_with: String
  directPay_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [BillingSummaryWhereInput!]
  OR: [BillingSummaryWhereInput!]
  NOT: [BillingSummaryWhereInput!]
}

input BillingSummaryWhereUniqueInput {
  id: ID
}

type Company {
  id: ID!
  user: User!
  companyName: String!
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type CompanyConnection {
  pageInfo: PageInfo!
  edges: [CompanyEdge]!
  aggregate: AggregateCompany!
}

input CompanyCreateInput {
  user: UserCreateOneWithoutCompanyInput!
  companyName: String!
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

input CompanyCreateManyWithoutUserInput {
  create: [CompanyCreateWithoutUserInput!]
  connect: [CompanyWhereUniqueInput!]
}

input CompanyCreateWithoutUserInput {
  companyName: String!
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

type CompanyEdge {
  node: Company!
  cursor: String!
}

enum CompanyOrderByInput {
  id_ASC
  id_DESC
  companyName_ASC
  companyName_DESC
  isMonthly_ASC
  isMonthly_DESC
  billingId_ASC
  billingId_DESC
  dueData_ASC
  dueData_DESC
  currentPlanId_ASC
  currentPlanId_DESC
  amount_ASC
  amount_DESC
  lastPayment_ASC
  lastPayment_DESC
  successOrFail_ASC
  successOrFail_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type CompanyPreviousValues {
  id: ID!
  companyName: String!
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input CompanyScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  companyName: String
  companyName_not: String
  companyName_in: [String!]
  companyName_not_in: [String!]
  companyName_lt: String
  companyName_lte: String
  companyName_gt: String
  companyName_gte: String
  companyName_contains: String
  companyName_not_contains: String
  companyName_starts_with: String
  companyName_not_starts_with: String
  companyName_ends_with: String
  companyName_not_ends_with: String
  isMonthly: Boolean
  isMonthly_not: Boolean
  billingId: String
  billingId_not: String
  billingId_in: [String!]
  billingId_not_in: [String!]
  billingId_lt: String
  billingId_lte: String
  billingId_gt: String
  billingId_gte: String
  billingId_contains: String
  billingId_not_contains: String
  billingId_starts_with: String
  billingId_not_starts_with: String
  billingId_ends_with: String
  billingId_not_ends_with: String
  dueData: String
  dueData_not: String
  dueData_in: [String!]
  dueData_not_in: [String!]
  dueData_lt: String
  dueData_lte: String
  dueData_gt: String
  dueData_gte: String
  dueData_contains: String
  dueData_not_contains: String
  dueData_starts_with: String
  dueData_not_starts_with: String
  dueData_ends_with: String
  dueData_not_ends_with: String
  currentPlanId: String
  currentPlanId_not: String
  currentPlanId_in: [String!]
  currentPlanId_not_in: [String!]
  currentPlanId_lt: String
  currentPlanId_lte: String
  currentPlanId_gt: String
  currentPlanId_gte: String
  currentPlanId_contains: String
  currentPlanId_not_contains: String
  currentPlanId_starts_with: String
  currentPlanId_not_starts_with: String
  currentPlanId_ends_with: String
  currentPlanId_not_ends_with: String
  amount: Float
  amount_not: Float
  amount_in: [Float!]
  amount_not_in: [Float!]
  amount_lt: Float
  amount_lte: Float
  amount_gt: Float
  amount_gte: Float
  lastPayment: String
  lastPayment_not: String
  lastPayment_in: [String!]
  lastPayment_not_in: [String!]
  lastPayment_lt: String
  lastPayment_lte: String
  lastPayment_gt: String
  lastPayment_gte: String
  lastPayment_contains: String
  lastPayment_not_contains: String
  lastPayment_starts_with: String
  lastPayment_not_starts_with: String
  lastPayment_ends_with: String
  lastPayment_not_ends_with: String
  successOrFail: String
  successOrFail_not: String
  successOrFail_in: [String!]
  successOrFail_not_in: [String!]
  successOrFail_lt: String
  successOrFail_lte: String
  successOrFail_gt: String
  successOrFail_gte: String
  successOrFail_contains: String
  successOrFail_not_contains: String
  successOrFail_starts_with: String
  successOrFail_not_starts_with: String
  successOrFail_ends_with: String
  successOrFail_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [CompanyScalarWhereInput!]
  OR: [CompanyScalarWhereInput!]
  NOT: [CompanyScalarWhereInput!]
}

type CompanySubscriptionPayload {
  mutation: MutationType!
  node: Company
  updatedFields: [String!]
  previousValues: CompanyPreviousValues
}

input CompanySubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: CompanyWhereInput
  AND: [CompanySubscriptionWhereInput!]
  OR: [CompanySubscriptionWhereInput!]
  NOT: [CompanySubscriptionWhereInput!]
}

input CompanyUpdateInput {
  user: UserUpdateOneRequiredWithoutCompanyInput
  companyName: String
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

input CompanyUpdateManyDataInput {
  companyName: String
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

input CompanyUpdateManyMutationInput {
  companyName: String
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

input CompanyUpdateManyWithoutUserInput {
  create: [CompanyCreateWithoutUserInput!]
  delete: [CompanyWhereUniqueInput!]
  connect: [CompanyWhereUniqueInput!]
  disconnect: [CompanyWhereUniqueInput!]
  update: [CompanyUpdateWithWhereUniqueWithoutUserInput!]
  upsert: [CompanyUpsertWithWhereUniqueWithoutUserInput!]
  deleteMany: [CompanyScalarWhereInput!]
  updateMany: [CompanyUpdateManyWithWhereNestedInput!]
}

input CompanyUpdateManyWithWhereNestedInput {
  where: CompanyScalarWhereInput!
  data: CompanyUpdateManyDataInput!
}

input CompanyUpdateWithoutUserDataInput {
  companyName: String
  isMonthly: Boolean
  billingId: String
  dueData: String
  currentPlanId: String
  amount: Float
  lastPayment: String
  successOrFail: String
  isDeleted: String
}

input CompanyUpdateWithWhereUniqueWithoutUserInput {
  where: CompanyWhereUniqueInput!
  data: CompanyUpdateWithoutUserDataInput!
}

input CompanyUpsertWithWhereUniqueWithoutUserInput {
  where: CompanyWhereUniqueInput!
  update: CompanyUpdateWithoutUserDataInput!
  create: CompanyCreateWithoutUserInput!
}

input CompanyWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  user: UserWhereInput
  companyName: String
  companyName_not: String
  companyName_in: [String!]
  companyName_not_in: [String!]
  companyName_lt: String
  companyName_lte: String
  companyName_gt: String
  companyName_gte: String
  companyName_contains: String
  companyName_not_contains: String
  companyName_starts_with: String
  companyName_not_starts_with: String
  companyName_ends_with: String
  companyName_not_ends_with: String
  isMonthly: Boolean
  isMonthly_not: Boolean
  billingId: String
  billingId_not: String
  billingId_in: [String!]
  billingId_not_in: [String!]
  billingId_lt: String
  billingId_lte: String
  billingId_gt: String
  billingId_gte: String
  billingId_contains: String
  billingId_not_contains: String
  billingId_starts_with: String
  billingId_not_starts_with: String
  billingId_ends_with: String
  billingId_not_ends_with: String
  dueData: String
  dueData_not: String
  dueData_in: [String!]
  dueData_not_in: [String!]
  dueData_lt: String
  dueData_lte: String
  dueData_gt: String
  dueData_gte: String
  dueData_contains: String
  dueData_not_contains: String
  dueData_starts_with: String
  dueData_not_starts_with: String
  dueData_ends_with: String
  dueData_not_ends_with: String
  currentPlanId: String
  currentPlanId_not: String
  currentPlanId_in: [String!]
  currentPlanId_not_in: [String!]
  currentPlanId_lt: String
  currentPlanId_lte: String
  currentPlanId_gt: String
  currentPlanId_gte: String
  currentPlanId_contains: String
  currentPlanId_not_contains: String
  currentPlanId_starts_with: String
  currentPlanId_not_starts_with: String
  currentPlanId_ends_with: String
  currentPlanId_not_ends_with: String
  amount: Float
  amount_not: Float
  amount_in: [Float!]
  amount_not_in: [Float!]
  amount_lt: Float
  amount_lte: Float
  amount_gt: Float
  amount_gte: Float
  lastPayment: String
  lastPayment_not: String
  lastPayment_in: [String!]
  lastPayment_not_in: [String!]
  lastPayment_lt: String
  lastPayment_lte: String
  lastPayment_gt: String
  lastPayment_gte: String
  lastPayment_contains: String
  lastPayment_not_contains: String
  lastPayment_starts_with: String
  lastPayment_not_starts_with: String
  lastPayment_ends_with: String
  lastPayment_not_ends_with: String
  successOrFail: String
  successOrFail_not: String
  successOrFail_in: [String!]
  successOrFail_not_in: [String!]
  successOrFail_lt: String
  successOrFail_lte: String
  successOrFail_gt: String
  successOrFail_gte: String
  successOrFail_contains: String
  successOrFail_not_contains: String
  successOrFail_starts_with: String
  successOrFail_not_starts_with: String
  successOrFail_ends_with: String
  successOrFail_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [CompanyWhereInput!]
  OR: [CompanyWhereInput!]
  NOT: [CompanyWhereInput!]
}

input CompanyWhereUniqueInput {
  id: ID
}

scalar DateTime

type defaultProject {
  id: ID!
  projectname: String!
  createdBy: User!
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type defaultProjectConnection {
  pageInfo: PageInfo!
  edges: [defaultProjectEdge]!
  aggregate: AggregatedefaultProject!
}

input defaultProjectCreateInput {
  projectname: String!
  createdBy: UserCreateOneWithoutProjectsInput!
  isDeleted: String
}

input defaultProjectCreateManyInput {
  create: [defaultProjectCreateInput!]
  connect: [defaultProjectWhereUniqueInput!]
}

input defaultProjectCreateManyWithoutCreatedByInput {
  create: [defaultProjectCreateWithoutCreatedByInput!]
  connect: [defaultProjectWhereUniqueInput!]
}

input defaultProjectCreateWithoutCreatedByInput {
  projectname: String!
  isDeleted: String
}

type defaultProjectEdge {
  node: defaultProject!
  cursor: String!
}

enum defaultProjectOrderByInput {
  id_ASC
  id_DESC
  projectname_ASC
  projectname_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type defaultProjectPreviousValues {
  id: ID!
  projectname: String!
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input defaultProjectScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  projectname: String
  projectname_not: String
  projectname_in: [String!]
  projectname_not_in: [String!]
  projectname_lt: String
  projectname_lte: String
  projectname_gt: String
  projectname_gte: String
  projectname_contains: String
  projectname_not_contains: String
  projectname_starts_with: String
  projectname_not_starts_with: String
  projectname_ends_with: String
  projectname_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [defaultProjectScalarWhereInput!]
  OR: [defaultProjectScalarWhereInput!]
  NOT: [defaultProjectScalarWhereInput!]
}

type defaultProjectSubscriptionPayload {
  mutation: MutationType!
  node: defaultProject
  updatedFields: [String!]
  previousValues: defaultProjectPreviousValues
}

input defaultProjectSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: defaultProjectWhereInput
  AND: [defaultProjectSubscriptionWhereInput!]
  OR: [defaultProjectSubscriptionWhereInput!]
  NOT: [defaultProjectSubscriptionWhereInput!]
}

input defaultProjectUpdateDataInput {
  projectname: String
  createdBy: UserUpdateOneRequiredWithoutProjectsInput
  isDeleted: String
}

input defaultProjectUpdateInput {
  projectname: String
  createdBy: UserUpdateOneRequiredWithoutProjectsInput
  isDeleted: String
}

input defaultProjectUpdateManyDataInput {
  projectname: String
  isDeleted: String
}

input defaultProjectUpdateManyInput {
  create: [defaultProjectCreateInput!]
  update: [defaultProjectUpdateWithWhereUniqueNestedInput!]
  upsert: [defaultProjectUpsertWithWhereUniqueNestedInput!]
  delete: [defaultProjectWhereUniqueInput!]
  connect: [defaultProjectWhereUniqueInput!]
  disconnect: [defaultProjectWhereUniqueInput!]
  deleteMany: [defaultProjectScalarWhereInput!]
  updateMany: [defaultProjectUpdateManyWithWhereNestedInput!]
}

input defaultProjectUpdateManyMutationInput {
  projectname: String
  isDeleted: String
}

input defaultProjectUpdateManyWithoutCreatedByInput {
  create: [defaultProjectCreateWithoutCreatedByInput!]
  delete: [defaultProjectWhereUniqueInput!]
  connect: [defaultProjectWhereUniqueInput!]
  disconnect: [defaultProjectWhereUniqueInput!]
  update: [defaultProjectUpdateWithWhereUniqueWithoutCreatedByInput!]
  upsert: [defaultProjectUpsertWithWhereUniqueWithoutCreatedByInput!]
  deleteMany: [defaultProjectScalarWhereInput!]
  updateMany: [defaultProjectUpdateManyWithWhereNestedInput!]
}

input defaultProjectUpdateManyWithWhereNestedInput {
  where: defaultProjectScalarWhereInput!
  data: defaultProjectUpdateManyDataInput!
}

input defaultProjectUpdateWithoutCreatedByDataInput {
  projectname: String
  isDeleted: String
}

input defaultProjectUpdateWithWhereUniqueNestedInput {
  where: defaultProjectWhereUniqueInput!
  data: defaultProjectUpdateDataInput!
}

input defaultProjectUpdateWithWhereUniqueWithoutCreatedByInput {
  where: defaultProjectWhereUniqueInput!
  data: defaultProjectUpdateWithoutCreatedByDataInput!
}

input defaultProjectUpsertWithWhereUniqueNestedInput {
  where: defaultProjectWhereUniqueInput!
  update: defaultProjectUpdateDataInput!
  create: defaultProjectCreateInput!
}

input defaultProjectUpsertWithWhereUniqueWithoutCreatedByInput {
  where: defaultProjectWhereUniqueInput!
  update: defaultProjectUpdateWithoutCreatedByDataInput!
  create: defaultProjectCreateWithoutCreatedByInput!
}

input defaultProjectWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  projectname: String
  projectname_not: String
  projectname_in: [String!]
  projectname_not_in: [String!]
  projectname_lt: String
  projectname_lte: String
  projectname_gt: String
  projectname_gte: String
  projectname_contains: String
  projectname_not_contains: String
  projectname_starts_with: String
  projectname_not_starts_with: String
  projectname_ends_with: String
  projectname_not_ends_with: String
  createdBy: UserWhereInput
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [defaultProjectWhereInput!]
  OR: [defaultProjectWhereInput!]
  NOT: [defaultProjectWhereInput!]
}

input defaultProjectWhereUniqueInput {
  id: ID
}

type defaultTemplate {
  id: ID!
  templatename: String!
  category1: String
  category2: String
  category3: String
  category4: String
  archive: String
  isDeleted: String
}

type defaultTemplateConnection {
  pageInfo: PageInfo!
  edges: [defaultTemplateEdge]!
  aggregate: AggregatedefaultTemplate!
}

input defaultTemplateCreateInput {
  templatename: String!
  category1: String
  category2: String
  category3: String
  category4: String
  archive: String
  isDeleted: String
}

type defaultTemplateEdge {
  node: defaultTemplate!
  cursor: String!
}

enum defaultTemplateOrderByInput {
  id_ASC
  id_DESC
  templatename_ASC
  templatename_DESC
  category1_ASC
  category1_DESC
  category2_ASC
  category2_DESC
  category3_ASC
  category3_DESC
  category4_ASC
  category4_DESC
  archive_ASC
  archive_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type defaultTemplatePreviousValues {
  id: ID!
  templatename: String!
  category1: String
  category2: String
  category3: String
  category4: String
  archive: String
  isDeleted: String
}

type defaultTemplateSubscriptionPayload {
  mutation: MutationType!
  node: defaultTemplate
  updatedFields: [String!]
  previousValues: defaultTemplatePreviousValues
}

input defaultTemplateSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: defaultTemplateWhereInput
  AND: [defaultTemplateSubscriptionWhereInput!]
  OR: [defaultTemplateSubscriptionWhereInput!]
  NOT: [defaultTemplateSubscriptionWhereInput!]
}

input defaultTemplateUpdateInput {
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  archive: String
  isDeleted: String
}

input defaultTemplateUpdateManyMutationInput {
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  archive: String
  isDeleted: String
}

input defaultTemplateWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  templatename: String
  templatename_not: String
  templatename_in: [String!]
  templatename_not_in: [String!]
  templatename_lt: String
  templatename_lte: String
  templatename_gt: String
  templatename_gte: String
  templatename_contains: String
  templatename_not_contains: String
  templatename_starts_with: String
  templatename_not_starts_with: String
  templatename_ends_with: String
  templatename_not_ends_with: String
  category1: String
  category1_not: String
  category1_in: [String!]
  category1_not_in: [String!]
  category1_lt: String
  category1_lte: String
  category1_gt: String
  category1_gte: String
  category1_contains: String
  category1_not_contains: String
  category1_starts_with: String
  category1_not_starts_with: String
  category1_ends_with: String
  category1_not_ends_with: String
  category2: String
  category2_not: String
  category2_in: [String!]
  category2_not_in: [String!]
  category2_lt: String
  category2_lte: String
  category2_gt: String
  category2_gte: String
  category2_contains: String
  category2_not_contains: String
  category2_starts_with: String
  category2_not_starts_with: String
  category2_ends_with: String
  category2_not_ends_with: String
  category3: String
  category3_not: String
  category3_in: [String!]
  category3_not_in: [String!]
  category3_lt: String
  category3_lte: String
  category3_gt: String
  category3_gte: String
  category3_contains: String
  category3_not_contains: String
  category3_starts_with: String
  category3_not_starts_with: String
  category3_ends_with: String
  category3_not_ends_with: String
  category4: String
  category4_not: String
  category4_in: [String!]
  category4_not_in: [String!]
  category4_lt: String
  category4_lte: String
  category4_gt: String
  category4_gte: String
  category4_contains: String
  category4_not_contains: String
  category4_starts_with: String
  category4_not_starts_with: String
  category4_ends_with: String
  category4_not_ends_with: String
  archive: String
  archive_not: String
  archive_in: [String!]
  archive_not_in: [String!]
  archive_lt: String
  archive_lte: String
  archive_gt: String
  archive_gte: String
  archive_contains: String
  archive_not_contains: String
  archive_starts_with: String
  archive_not_starts_with: String
  archive_ends_with: String
  archive_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  AND: [defaultTemplateWhereInput!]
  OR: [defaultTemplateWhereInput!]
  NOT: [defaultTemplateWhereInput!]
}

input defaultTemplateWhereUniqueInput {
  id: ID
}

type InviteToJointRetro {
  id: ID!
  retroId: String!
  retro: Retro!
  email: String!
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type InviteToJointRetroConnection {
  pageInfo: PageInfo!
  edges: [InviteToJointRetroEdge]!
  aggregate: AggregateInviteToJointRetro!
}

input InviteToJointRetroCreateInput {
  retroId: String!
  retro: RetroCreateOneWithoutInviteToJointRetrosInput!
  email: String!
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

input InviteToJointRetroCreateManyWithoutRetroInput {
  create: [InviteToJointRetroCreateWithoutRetroInput!]
  connect: [InviteToJointRetroWhereUniqueInput!]
}

input InviteToJointRetroCreateWithoutRetroInput {
  retroId: String!
  email: String!
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

type InviteToJointRetroEdge {
  node: InviteToJointRetro!
  cursor: String!
}

enum InviteToJointRetroOrderByInput {
  id_ASC
  id_DESC
  retroId_ASC
  retroId_DESC
  email_ASC
  email_DESC
  isAdmin_ASC
  isAdmin_DESC
  url_ASC
  url_DESC
  startDate_ASC
  startDate_DESC
  endDate_ASC
  endDate_DESC
  startTime_ASC
  startTime_DESC
  endTime_ASC
  endTime_DESC
  inputWeek_ASC
  inputWeek_DESC
  repeatEvery_ASC
  repeatEvery_DESC
  roomCode_ASC
  roomCode_DESC
  shareLink_ASC
  shareLink_DESC
  isJoined_ASC
  isJoined_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type InviteToJointRetroPreviousValues {
  id: ID!
  retroId: String!
  email: String!
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input InviteToJointRetroScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  retroId: String
  retroId_not: String
  retroId_in: [String!]
  retroId_not_in: [String!]
  retroId_lt: String
  retroId_lte: String
  retroId_gt: String
  retroId_gte: String
  retroId_contains: String
  retroId_not_contains: String
  retroId_starts_with: String
  retroId_not_starts_with: String
  retroId_ends_with: String
  retroId_not_ends_with: String
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  isAdmin: String
  isAdmin_not: String
  isAdmin_in: [String!]
  isAdmin_not_in: [String!]
  isAdmin_lt: String
  isAdmin_lte: String
  isAdmin_gt: String
  isAdmin_gte: String
  isAdmin_contains: String
  isAdmin_not_contains: String
  isAdmin_starts_with: String
  isAdmin_not_starts_with: String
  isAdmin_ends_with: String
  isAdmin_not_ends_with: String
  url: String
  url_not: String
  url_in: [String!]
  url_not_in: [String!]
  url_lt: String
  url_lte: String
  url_gt: String
  url_gte: String
  url_contains: String
  url_not_contains: String
  url_starts_with: String
  url_not_starts_with: String
  url_ends_with: String
  url_not_ends_with: String
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  startTime: String
  startTime_not: String
  startTime_in: [String!]
  startTime_not_in: [String!]
  startTime_lt: String
  startTime_lte: String
  startTime_gt: String
  startTime_gte: String
  startTime_contains: String
  startTime_not_contains: String
  startTime_starts_with: String
  startTime_not_starts_with: String
  startTime_ends_with: String
  startTime_not_ends_with: String
  endTime: String
  endTime_not: String
  endTime_in: [String!]
  endTime_not_in: [String!]
  endTime_lt: String
  endTime_lte: String
  endTime_gt: String
  endTime_gte: String
  endTime_contains: String
  endTime_not_contains: String
  endTime_starts_with: String
  endTime_not_starts_with: String
  endTime_ends_with: String
  endTime_not_ends_with: String
  inputWeek: String
  inputWeek_not: String
  inputWeek_in: [String!]
  inputWeek_not_in: [String!]
  inputWeek_lt: String
  inputWeek_lte: String
  inputWeek_gt: String
  inputWeek_gte: String
  inputWeek_contains: String
  inputWeek_not_contains: String
  inputWeek_starts_with: String
  inputWeek_not_starts_with: String
  inputWeek_ends_with: String
  inputWeek_not_ends_with: String
  repeatEvery: String
  repeatEvery_not: String
  repeatEvery_in: [String!]
  repeatEvery_not_in: [String!]
  repeatEvery_lt: String
  repeatEvery_lte: String
  repeatEvery_gt: String
  repeatEvery_gte: String
  repeatEvery_contains: String
  repeatEvery_not_contains: String
  repeatEvery_starts_with: String
  repeatEvery_not_starts_with: String
  repeatEvery_ends_with: String
  repeatEvery_not_ends_with: String
  roomCode: String
  roomCode_not: String
  roomCode_in: [String!]
  roomCode_not_in: [String!]
  roomCode_lt: String
  roomCode_lte: String
  roomCode_gt: String
  roomCode_gte: String
  roomCode_contains: String
  roomCode_not_contains: String
  roomCode_starts_with: String
  roomCode_not_starts_with: String
  roomCode_ends_with: String
  roomCode_not_ends_with: String
  shareLink: String
  shareLink_not: String
  shareLink_in: [String!]
  shareLink_not_in: [String!]
  shareLink_lt: String
  shareLink_lte: String
  shareLink_gt: String
  shareLink_gte: String
  shareLink_contains: String
  shareLink_not_contains: String
  shareLink_starts_with: String
  shareLink_not_starts_with: String
  shareLink_ends_with: String
  shareLink_not_ends_with: String
  isJoined: String
  isJoined_not: String
  isJoined_in: [String!]
  isJoined_not_in: [String!]
  isJoined_lt: String
  isJoined_lte: String
  isJoined_gt: String
  isJoined_gte: String
  isJoined_contains: String
  isJoined_not_contains: String
  isJoined_starts_with: String
  isJoined_not_starts_with: String
  isJoined_ends_with: String
  isJoined_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [InviteToJointRetroScalarWhereInput!]
  OR: [InviteToJointRetroScalarWhereInput!]
  NOT: [InviteToJointRetroScalarWhereInput!]
}

type InviteToJointRetroSubscriptionPayload {
  mutation: MutationType!
  node: InviteToJointRetro
  updatedFields: [String!]
  previousValues: InviteToJointRetroPreviousValues
}

input InviteToJointRetroSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: InviteToJointRetroWhereInput
  AND: [InviteToJointRetroSubscriptionWhereInput!]
  OR: [InviteToJointRetroSubscriptionWhereInput!]
  NOT: [InviteToJointRetroSubscriptionWhereInput!]
}

input InviteToJointRetroUpdateInput {
  retroId: String
  retro: RetroUpdateOneRequiredWithoutInviteToJointRetrosInput
  email: String
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

input InviteToJointRetroUpdateManyDataInput {
  retroId: String
  email: String
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

input InviteToJointRetroUpdateManyMutationInput {
  retroId: String
  email: String
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

input InviteToJointRetroUpdateManyWithoutRetroInput {
  create: [InviteToJointRetroCreateWithoutRetroInput!]
  delete: [InviteToJointRetroWhereUniqueInput!]
  connect: [InviteToJointRetroWhereUniqueInput!]
  disconnect: [InviteToJointRetroWhereUniqueInput!]
  update: [InviteToJointRetroUpdateWithWhereUniqueWithoutRetroInput!]
  upsert: [InviteToJointRetroUpsertWithWhereUniqueWithoutRetroInput!]
  deleteMany: [InviteToJointRetroScalarWhereInput!]
  updateMany: [InviteToJointRetroUpdateManyWithWhereNestedInput!]
}

input InviteToJointRetroUpdateManyWithWhereNestedInput {
  where: InviteToJointRetroScalarWhereInput!
  data: InviteToJointRetroUpdateManyDataInput!
}

input InviteToJointRetroUpdateWithoutRetroDataInput {
  retroId: String
  email: String
  isAdmin: String
  url: String
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  inputWeek: String
  repeatEvery: String
  roomCode: String
  shareLink: String
  isJoined: String
  isDeleted: String
}

input InviteToJointRetroUpdateWithWhereUniqueWithoutRetroInput {
  where: InviteToJointRetroWhereUniqueInput!
  data: InviteToJointRetroUpdateWithoutRetroDataInput!
}

input InviteToJointRetroUpsertWithWhereUniqueWithoutRetroInput {
  where: InviteToJointRetroWhereUniqueInput!
  update: InviteToJointRetroUpdateWithoutRetroDataInput!
  create: InviteToJointRetroCreateWithoutRetroInput!
}

input InviteToJointRetroWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  retroId: String
  retroId_not: String
  retroId_in: [String!]
  retroId_not_in: [String!]
  retroId_lt: String
  retroId_lte: String
  retroId_gt: String
  retroId_gte: String
  retroId_contains: String
  retroId_not_contains: String
  retroId_starts_with: String
  retroId_not_starts_with: String
  retroId_ends_with: String
  retroId_not_ends_with: String
  retro: RetroWhereInput
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  isAdmin: String
  isAdmin_not: String
  isAdmin_in: [String!]
  isAdmin_not_in: [String!]
  isAdmin_lt: String
  isAdmin_lte: String
  isAdmin_gt: String
  isAdmin_gte: String
  isAdmin_contains: String
  isAdmin_not_contains: String
  isAdmin_starts_with: String
  isAdmin_not_starts_with: String
  isAdmin_ends_with: String
  isAdmin_not_ends_with: String
  url: String
  url_not: String
  url_in: [String!]
  url_not_in: [String!]
  url_lt: String
  url_lte: String
  url_gt: String
  url_gte: String
  url_contains: String
  url_not_contains: String
  url_starts_with: String
  url_not_starts_with: String
  url_ends_with: String
  url_not_ends_with: String
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  startTime: String
  startTime_not: String
  startTime_in: [String!]
  startTime_not_in: [String!]
  startTime_lt: String
  startTime_lte: String
  startTime_gt: String
  startTime_gte: String
  startTime_contains: String
  startTime_not_contains: String
  startTime_starts_with: String
  startTime_not_starts_with: String
  startTime_ends_with: String
  startTime_not_ends_with: String
  endTime: String
  endTime_not: String
  endTime_in: [String!]
  endTime_not_in: [String!]
  endTime_lt: String
  endTime_lte: String
  endTime_gt: String
  endTime_gte: String
  endTime_contains: String
  endTime_not_contains: String
  endTime_starts_with: String
  endTime_not_starts_with: String
  endTime_ends_with: String
  endTime_not_ends_with: String
  inputWeek: String
  inputWeek_not: String
  inputWeek_in: [String!]
  inputWeek_not_in: [String!]
  inputWeek_lt: String
  inputWeek_lte: String
  inputWeek_gt: String
  inputWeek_gte: String
  inputWeek_contains: String
  inputWeek_not_contains: String
  inputWeek_starts_with: String
  inputWeek_not_starts_with: String
  inputWeek_ends_with: String
  inputWeek_not_ends_with: String
  repeatEvery: String
  repeatEvery_not: String
  repeatEvery_in: [String!]
  repeatEvery_not_in: [String!]
  repeatEvery_lt: String
  repeatEvery_lte: String
  repeatEvery_gt: String
  repeatEvery_gte: String
  repeatEvery_contains: String
  repeatEvery_not_contains: String
  repeatEvery_starts_with: String
  repeatEvery_not_starts_with: String
  repeatEvery_ends_with: String
  repeatEvery_not_ends_with: String
  roomCode: String
  roomCode_not: String
  roomCode_in: [String!]
  roomCode_not_in: [String!]
  roomCode_lt: String
  roomCode_lte: String
  roomCode_gt: String
  roomCode_gte: String
  roomCode_contains: String
  roomCode_not_contains: String
  roomCode_starts_with: String
  roomCode_not_starts_with: String
  roomCode_ends_with: String
  roomCode_not_ends_with: String
  shareLink: String
  shareLink_not: String
  shareLink_in: [String!]
  shareLink_not_in: [String!]
  shareLink_lt: String
  shareLink_lte: String
  shareLink_gt: String
  shareLink_gte: String
  shareLink_contains: String
  shareLink_not_contains: String
  shareLink_starts_with: String
  shareLink_not_starts_with: String
  shareLink_ends_with: String
  shareLink_not_ends_with: String
  isJoined: String
  isJoined_not: String
  isJoined_in: [String!]
  isJoined_not_in: [String!]
  isJoined_lt: String
  isJoined_lte: String
  isJoined_gt: String
  isJoined_gte: String
  isJoined_contains: String
  isJoined_not_contains: String
  isJoined_starts_with: String
  isJoined_not_starts_with: String
  isJoined_ends_with: String
  isJoined_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [InviteToJointRetroWhereInput!]
  OR: [InviteToJointRetroWhereInput!]
  NOT: [InviteToJointRetroWhereInput!]
}

input InviteToJointRetroWhereUniqueInput {
  id: ID
}

scalar Long

type Mutation {
  createAddCard(data: AddCardCreateInput!): AddCard!
  updateAddCard(data: AddCardUpdateInput!, where: AddCardWhereUniqueInput!): AddCard
  updateManyAddCards(data: AddCardUpdateManyMutationInput!, where: AddCardWhereInput): BatchPayload!
  upsertAddCard(where: AddCardWhereUniqueInput!, create: AddCardCreateInput!, update: AddCardUpdateInput!): AddCard!
  deleteAddCard(where: AddCardWhereUniqueInput!): AddCard
  deleteManyAddCards(where: AddCardWhereInput): BatchPayload!
  createBillingSummary(data: BillingSummaryCreateInput!): BillingSummary!
  updateBillingSummary(data: BillingSummaryUpdateInput!, where: BillingSummaryWhereUniqueInput!): BillingSummary
  updateManyBillingSummaries(data: BillingSummaryUpdateManyMutationInput!, where: BillingSummaryWhereInput): BatchPayload!
  upsertBillingSummary(where: BillingSummaryWhereUniqueInput!, create: BillingSummaryCreateInput!, update: BillingSummaryUpdateInput!): BillingSummary!
  deleteBillingSummary(where: BillingSummaryWhereUniqueInput!): BillingSummary
  deleteManyBillingSummaries(where: BillingSummaryWhereInput): BatchPayload!
  createCompany(data: CompanyCreateInput!): Company!
  updateCompany(data: CompanyUpdateInput!, where: CompanyWhereUniqueInput!): Company
  updateManyCompanies(data: CompanyUpdateManyMutationInput!, where: CompanyWhereInput): BatchPayload!
  upsertCompany(where: CompanyWhereUniqueInput!, create: CompanyCreateInput!, update: CompanyUpdateInput!): Company!
  deleteCompany(where: CompanyWhereUniqueInput!): Company
  deleteManyCompanies(where: CompanyWhereInput): BatchPayload!
  createInviteToJointRetro(data: InviteToJointRetroCreateInput!): InviteToJointRetro!
  updateInviteToJointRetro(data: InviteToJointRetroUpdateInput!, where: InviteToJointRetroWhereUniqueInput!): InviteToJointRetro
  updateManyInviteToJointRetroes(data: InviteToJointRetroUpdateManyMutationInput!, where: InviteToJointRetroWhereInput): BatchPayload!
  upsertInviteToJointRetro(where: InviteToJointRetroWhereUniqueInput!, create: InviteToJointRetroCreateInput!, update: InviteToJointRetroUpdateInput!): InviteToJointRetro!
  deleteInviteToJointRetro(where: InviteToJointRetroWhereUniqueInput!): InviteToJointRetro
  deleteManyInviteToJointRetroes(where: InviteToJointRetroWhereInput): BatchPayload!
  createPlan(data: PlanCreateInput!): Plan!
  updatePlan(data: PlanUpdateInput!, where: PlanWhereUniqueInput!): Plan
  updateManyPlans(data: PlanUpdateManyMutationInput!, where: PlanWhereInput): BatchPayload!
  upsertPlan(where: PlanWhereUniqueInput!, create: PlanCreateInput!, update: PlanUpdateInput!): Plan!
  deletePlan(where: PlanWhereUniqueInput!): Plan
  deleteManyPlans(where: PlanWhereInput): BatchPayload!
  createPromoCode(data: PromoCodeCreateInput!): PromoCode!
  updatePromoCode(data: PromoCodeUpdateInput!, where: PromoCodeWhereUniqueInput!): PromoCode
  updateManyPromoCodes(data: PromoCodeUpdateManyMutationInput!, where: PromoCodeWhereInput): BatchPayload!
  upsertPromoCode(where: PromoCodeWhereUniqueInput!, create: PromoCodeCreateInput!, update: PromoCodeUpdateInput!): PromoCode!
  deletePromoCode(where: PromoCodeWhereUniqueInput!): PromoCode
  deleteManyPromoCodes(where: PromoCodeWhereInput): BatchPayload!
  createRetro(data: RetroCreateInput!): Retro!
  updateRetro(data: RetroUpdateInput!, where: RetroWhereUniqueInput!): Retro
  updateManyRetroes(data: RetroUpdateManyMutationInput!, where: RetroWhereInput): BatchPayload!
  upsertRetro(where: RetroWhereUniqueInput!, create: RetroCreateInput!, update: RetroUpdateInput!): Retro!
  deleteRetro(where: RetroWhereUniqueInput!): Retro
  deleteManyRetroes(where: RetroWhereInput): BatchPayload!
  createThought(data: ThoughtCreateInput!): Thought!
  updateThought(data: ThoughtUpdateInput!, where: ThoughtWhereUniqueInput!): Thought
  updateManyThoughts(data: ThoughtUpdateManyMutationInput!, where: ThoughtWhereInput): BatchPayload!
  upsertThought(where: ThoughtWhereUniqueInput!, create: ThoughtCreateInput!, update: ThoughtUpdateInput!): Thought!
  deleteThought(where: ThoughtWhereUniqueInput!): Thought
  deleteManyThoughts(where: ThoughtWhereInput): BatchPayload!
  createUser(data: UserCreateInput!): User!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  deleteUser(where: UserWhereUniqueInput!): User
  deleteManyUsers(where: UserWhereInput): BatchPayload!
  createdefaultProject(data: defaultProjectCreateInput!): defaultProject!
  updatedefaultProject(data: defaultProjectUpdateInput!, where: defaultProjectWhereUniqueInput!): defaultProject
  updateManydefaultProjects(data: defaultProjectUpdateManyMutationInput!, where: defaultProjectWhereInput): BatchPayload!
  upsertdefaultProject(where: defaultProjectWhereUniqueInput!, create: defaultProjectCreateInput!, update: defaultProjectUpdateInput!): defaultProject!
  deletedefaultProject(where: defaultProjectWhereUniqueInput!): defaultProject
  deleteManydefaultProjects(where: defaultProjectWhereInput): BatchPayload!
  createdefaultTemplate(data: defaultTemplateCreateInput!): defaultTemplate!
  updatedefaultTemplate(data: defaultTemplateUpdateInput!, where: defaultTemplateWhereUniqueInput!): defaultTemplate
  updateManydefaultTemplates(data: defaultTemplateUpdateManyMutationInput!, where: defaultTemplateWhereInput): BatchPayload!
  upsertdefaultTemplate(where: defaultTemplateWhereUniqueInput!, create: defaultTemplateCreateInput!, update: defaultTemplateUpdateInput!): defaultTemplate!
  deletedefaultTemplate(where: defaultTemplateWhereUniqueInput!): defaultTemplate
  deleteManydefaultTemplates(where: defaultTemplateWhereInput): BatchPayload!
  createuserTemplate(data: userTemplateCreateInput!): userTemplate!
  updateuserTemplate(data: userTemplateUpdateInput!, where: userTemplateWhereUniqueInput!): userTemplate
  updateManyuserTemplates(data: userTemplateUpdateManyMutationInput!, where: userTemplateWhereInput): BatchPayload!
  upsertuserTemplate(where: userTemplateWhereUniqueInput!, create: userTemplateCreateInput!, update: userTemplateUpdateInput!): userTemplate!
  deleteuserTemplate(where: userTemplateWhereUniqueInput!): userTemplate
  deleteManyuserTemplates(where: userTemplateWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Plan {
  id: ID!
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type PlanConnection {
  pageInfo: PageInfo!
  edges: [PlanEdge]!
  aggregate: AggregatePlan!
}

input PlanCreateInput {
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
}

input PlanCreateOneInput {
  create: PlanCreateInput
  connect: PlanWhereUniqueInput
}

type PlanEdge {
  node: Plan!
  cursor: String!
}

enum PlanOrderByInput {
  id_ASC
  id_DESC
  planName_ASC
  planName_DESC
  currency_ASC
  currency_DESC
  amount_ASC
  amount_DESC
  maxRetro_ASC
  maxRetro_DESC
  maxRetroAdmin_ASC
  maxRetroAdmin_DESC
  maxCompanyAdmin_ASC
  maxCompanyAdmin_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PlanPreviousValues {
  id: ID!
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type PlanSubscriptionPayload {
  mutation: MutationType!
  node: Plan
  updatedFields: [String!]
  previousValues: PlanPreviousValues
}

input PlanSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: PlanWhereInput
  AND: [PlanSubscriptionWhereInput!]
  OR: [PlanSubscriptionWhereInput!]
  NOT: [PlanSubscriptionWhereInput!]
}

input PlanUpdateDataInput {
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
}

input PlanUpdateInput {
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
}

input PlanUpdateManyMutationInput {
  planName: String
  currency: String
  amount: Int
  maxRetro: Int
  maxRetroAdmin: Int
  maxCompanyAdmin: Int
  isDeleted: String
}

input PlanUpdateOneInput {
  create: PlanCreateInput
  update: PlanUpdateDataInput
  upsert: PlanUpsertNestedInput
  delete: Boolean
  disconnect: Boolean
  connect: PlanWhereUniqueInput
}

input PlanUpsertNestedInput {
  update: PlanUpdateDataInput!
  create: PlanCreateInput!
}

input PlanWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  planName: String
  planName_not: String
  planName_in: [String!]
  planName_not_in: [String!]
  planName_lt: String
  planName_lte: String
  planName_gt: String
  planName_gte: String
  planName_contains: String
  planName_not_contains: String
  planName_starts_with: String
  planName_not_starts_with: String
  planName_ends_with: String
  planName_not_ends_with: String
  currency: String
  currency_not: String
  currency_in: [String!]
  currency_not_in: [String!]
  currency_lt: String
  currency_lte: String
  currency_gt: String
  currency_gte: String
  currency_contains: String
  currency_not_contains: String
  currency_starts_with: String
  currency_not_starts_with: String
  currency_ends_with: String
  currency_not_ends_with: String
  amount: Int
  amount_not: Int
  amount_in: [Int!]
  amount_not_in: [Int!]
  amount_lt: Int
  amount_lte: Int
  amount_gt: Int
  amount_gte: Int
  maxRetro: Int
  maxRetro_not: Int
  maxRetro_in: [Int!]
  maxRetro_not_in: [Int!]
  maxRetro_lt: Int
  maxRetro_lte: Int
  maxRetro_gt: Int
  maxRetro_gte: Int
  maxRetroAdmin: Int
  maxRetroAdmin_not: Int
  maxRetroAdmin_in: [Int!]
  maxRetroAdmin_not_in: [Int!]
  maxRetroAdmin_lt: Int
  maxRetroAdmin_lte: Int
  maxRetroAdmin_gt: Int
  maxRetroAdmin_gte: Int
  maxCompanyAdmin: Int
  maxCompanyAdmin_not: Int
  maxCompanyAdmin_in: [Int!]
  maxCompanyAdmin_not_in: [Int!]
  maxCompanyAdmin_lt: Int
  maxCompanyAdmin_lte: Int
  maxCompanyAdmin_gt: Int
  maxCompanyAdmin_gte: Int
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [PlanWhereInput!]
  OR: [PlanWhereInput!]
  NOT: [PlanWhereInput!]
}

input PlanWhereUniqueInput {
  id: ID
}

type PromoCode {
  id: ID!
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type PromoCodeConnection {
  pageInfo: PageInfo!
  edges: [PromoCodeEdge]!
  aggregate: AggregatePromoCode!
}

input PromoCodeCreateInput {
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
}

input PromoCodeCreateManyInput {
  create: [PromoCodeCreateInput!]
  connect: [PromoCodeWhereUniqueInput!]
}

input PromoCodeCreateOneInput {
  create: PromoCodeCreateInput
  connect: PromoCodeWhereUniqueInput
}

type PromoCodeEdge {
  node: PromoCode!
  cursor: String!
}

enum PromoCodeOrderByInput {
  id_ASC
  id_DESC
  userId_ASC
  userId_DESC
  promoCode_ASC
  promoCode_DESC
  target_ASC
  target_DESC
  discountValue_ASC
  discountValue_DESC
  limitValue_ASC
  limitValue_DESC
  repeat_ASC
  repeat_DESC
  startDate_ASC
  startDate_DESC
  endDate_ASC
  endDate_DESC
  usedBy_ASC
  usedBy_DESC
  limit_ASC
  limit_DESC
  unit_ASC
  unit_DESC
  isVaild_ASC
  isVaild_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PromoCodePreviousValues {
  id: ID!
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input PromoCodeScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  promoCode: String
  promoCode_not: String
  promoCode_in: [String!]
  promoCode_not_in: [String!]
  promoCode_lt: String
  promoCode_lte: String
  promoCode_gt: String
  promoCode_gte: String
  promoCode_contains: String
  promoCode_not_contains: String
  promoCode_starts_with: String
  promoCode_not_starts_with: String
  promoCode_ends_with: String
  promoCode_not_ends_with: String
  target: String
  target_not: String
  target_in: [String!]
  target_not_in: [String!]
  target_lt: String
  target_lte: String
  target_gt: String
  target_gte: String
  target_contains: String
  target_not_contains: String
  target_starts_with: String
  target_not_starts_with: String
  target_ends_with: String
  target_not_ends_with: String
  discountValue: String
  discountValue_not: String
  discountValue_in: [String!]
  discountValue_not_in: [String!]
  discountValue_lt: String
  discountValue_lte: String
  discountValue_gt: String
  discountValue_gte: String
  discountValue_contains: String
  discountValue_not_contains: String
  discountValue_starts_with: String
  discountValue_not_starts_with: String
  discountValue_ends_with: String
  discountValue_not_ends_with: String
  limitValue: String
  limitValue_not: String
  limitValue_in: [String!]
  limitValue_not_in: [String!]
  limitValue_lt: String
  limitValue_lte: String
  limitValue_gt: String
  limitValue_gte: String
  limitValue_contains: String
  limitValue_not_contains: String
  limitValue_starts_with: String
  limitValue_not_starts_with: String
  limitValue_ends_with: String
  limitValue_not_ends_with: String
  repeat: String
  repeat_not: String
  repeat_in: [String!]
  repeat_not_in: [String!]
  repeat_lt: String
  repeat_lte: String
  repeat_gt: String
  repeat_gte: String
  repeat_contains: String
  repeat_not_contains: String
  repeat_starts_with: String
  repeat_not_starts_with: String
  repeat_ends_with: String
  repeat_not_ends_with: String
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  usedBy: String
  usedBy_not: String
  usedBy_in: [String!]
  usedBy_not_in: [String!]
  usedBy_lt: String
  usedBy_lte: String
  usedBy_gt: String
  usedBy_gte: String
  usedBy_contains: String
  usedBy_not_contains: String
  usedBy_starts_with: String
  usedBy_not_starts_with: String
  usedBy_ends_with: String
  usedBy_not_ends_with: String
  limit: String
  limit_not: String
  limit_in: [String!]
  limit_not_in: [String!]
  limit_lt: String
  limit_lte: String
  limit_gt: String
  limit_gte: String
  limit_contains: String
  limit_not_contains: String
  limit_starts_with: String
  limit_not_starts_with: String
  limit_ends_with: String
  limit_not_ends_with: String
  unit: String
  unit_not: String
  unit_in: [String!]
  unit_not_in: [String!]
  unit_lt: String
  unit_lte: String
  unit_gt: String
  unit_gte: String
  unit_contains: String
  unit_not_contains: String
  unit_starts_with: String
  unit_not_starts_with: String
  unit_ends_with: String
  unit_not_ends_with: String
  isVaild: String
  isVaild_not: String
  isVaild_in: [String!]
  isVaild_not_in: [String!]
  isVaild_lt: String
  isVaild_lte: String
  isVaild_gt: String
  isVaild_gte: String
  isVaild_contains: String
  isVaild_not_contains: String
  isVaild_starts_with: String
  isVaild_not_starts_with: String
  isVaild_ends_with: String
  isVaild_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [PromoCodeScalarWhereInput!]
  OR: [PromoCodeScalarWhereInput!]
  NOT: [PromoCodeScalarWhereInput!]
}

type PromoCodeSubscriptionPayload {
  mutation: MutationType!
  node: PromoCode
  updatedFields: [String!]
  previousValues: PromoCodePreviousValues
}

input PromoCodeSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: PromoCodeWhereInput
  AND: [PromoCodeSubscriptionWhereInput!]
  OR: [PromoCodeSubscriptionWhereInput!]
  NOT: [PromoCodeSubscriptionWhereInput!]
}

input PromoCodeUpdateDataInput {
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
}

input PromoCodeUpdateInput {
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
}

input PromoCodeUpdateManyDataInput {
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
}

input PromoCodeUpdateManyInput {
  create: [PromoCodeCreateInput!]
  update: [PromoCodeUpdateWithWhereUniqueNestedInput!]
  upsert: [PromoCodeUpsertWithWhereUniqueNestedInput!]
  delete: [PromoCodeWhereUniqueInput!]
  connect: [PromoCodeWhereUniqueInput!]
  disconnect: [PromoCodeWhereUniqueInput!]
  deleteMany: [PromoCodeScalarWhereInput!]
  updateMany: [PromoCodeUpdateManyWithWhereNestedInput!]
}

input PromoCodeUpdateManyMutationInput {
  userId: String
  promoCode: String
  target: String
  discountValue: String
  limitValue: String
  repeat: String
  startDate: String
  endDate: String
  usedBy: String
  limit: String
  unit: String
  isVaild: String
  isDeleted: String
}

input PromoCodeUpdateManyWithWhereNestedInput {
  where: PromoCodeScalarWhereInput!
  data: PromoCodeUpdateManyDataInput!
}

input PromoCodeUpdateOneInput {
  create: PromoCodeCreateInput
  update: PromoCodeUpdateDataInput
  upsert: PromoCodeUpsertNestedInput
  delete: Boolean
  disconnect: Boolean
  connect: PromoCodeWhereUniqueInput
}

input PromoCodeUpdateWithWhereUniqueNestedInput {
  where: PromoCodeWhereUniqueInput!
  data: PromoCodeUpdateDataInput!
}

input PromoCodeUpsertNestedInput {
  update: PromoCodeUpdateDataInput!
  create: PromoCodeCreateInput!
}

input PromoCodeUpsertWithWhereUniqueNestedInput {
  where: PromoCodeWhereUniqueInput!
  update: PromoCodeUpdateDataInput!
  create: PromoCodeCreateInput!
}

input PromoCodeWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userId: String
  userId_not: String
  userId_in: [String!]
  userId_not_in: [String!]
  userId_lt: String
  userId_lte: String
  userId_gt: String
  userId_gte: String
  userId_contains: String
  userId_not_contains: String
  userId_starts_with: String
  userId_not_starts_with: String
  userId_ends_with: String
  userId_not_ends_with: String
  promoCode: String
  promoCode_not: String
  promoCode_in: [String!]
  promoCode_not_in: [String!]
  promoCode_lt: String
  promoCode_lte: String
  promoCode_gt: String
  promoCode_gte: String
  promoCode_contains: String
  promoCode_not_contains: String
  promoCode_starts_with: String
  promoCode_not_starts_with: String
  promoCode_ends_with: String
  promoCode_not_ends_with: String
  target: String
  target_not: String
  target_in: [String!]
  target_not_in: [String!]
  target_lt: String
  target_lte: String
  target_gt: String
  target_gte: String
  target_contains: String
  target_not_contains: String
  target_starts_with: String
  target_not_starts_with: String
  target_ends_with: String
  target_not_ends_with: String
  discountValue: String
  discountValue_not: String
  discountValue_in: [String!]
  discountValue_not_in: [String!]
  discountValue_lt: String
  discountValue_lte: String
  discountValue_gt: String
  discountValue_gte: String
  discountValue_contains: String
  discountValue_not_contains: String
  discountValue_starts_with: String
  discountValue_not_starts_with: String
  discountValue_ends_with: String
  discountValue_not_ends_with: String
  limitValue: String
  limitValue_not: String
  limitValue_in: [String!]
  limitValue_not_in: [String!]
  limitValue_lt: String
  limitValue_lte: String
  limitValue_gt: String
  limitValue_gte: String
  limitValue_contains: String
  limitValue_not_contains: String
  limitValue_starts_with: String
  limitValue_not_starts_with: String
  limitValue_ends_with: String
  limitValue_not_ends_with: String
  repeat: String
  repeat_not: String
  repeat_in: [String!]
  repeat_not_in: [String!]
  repeat_lt: String
  repeat_lte: String
  repeat_gt: String
  repeat_gte: String
  repeat_contains: String
  repeat_not_contains: String
  repeat_starts_with: String
  repeat_not_starts_with: String
  repeat_ends_with: String
  repeat_not_ends_with: String
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  usedBy: String
  usedBy_not: String
  usedBy_in: [String!]
  usedBy_not_in: [String!]
  usedBy_lt: String
  usedBy_lte: String
  usedBy_gt: String
  usedBy_gte: String
  usedBy_contains: String
  usedBy_not_contains: String
  usedBy_starts_with: String
  usedBy_not_starts_with: String
  usedBy_ends_with: String
  usedBy_not_ends_with: String
  limit: String
  limit_not: String
  limit_in: [String!]
  limit_not_in: [String!]
  limit_lt: String
  limit_lte: String
  limit_gt: String
  limit_gte: String
  limit_contains: String
  limit_not_contains: String
  limit_starts_with: String
  limit_not_starts_with: String
  limit_ends_with: String
  limit_not_ends_with: String
  unit: String
  unit_not: String
  unit_in: [String!]
  unit_not_in: [String!]
  unit_lt: String
  unit_lte: String
  unit_gt: String
  unit_gte: String
  unit_contains: String
  unit_not_contains: String
  unit_starts_with: String
  unit_not_starts_with: String
  unit_ends_with: String
  unit_not_ends_with: String
  isVaild: String
  isVaild_not: String
  isVaild_in: [String!]
  isVaild_not_in: [String!]
  isVaild_lt: String
  isVaild_lte: String
  isVaild_gt: String
  isVaild_gte: String
  isVaild_contains: String
  isVaild_not_contains: String
  isVaild_starts_with: String
  isVaild_not_starts_with: String
  isVaild_ends_with: String
  isVaild_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [PromoCodeWhereInput!]
  OR: [PromoCodeWhereInput!]
  NOT: [PromoCodeWhereInput!]
}

input PromoCodeWhereUniqueInput {
  id: ID
}

type Query {
  addCard(where: AddCardWhereUniqueInput!): AddCard
  addCards(where: AddCardWhereInput, orderBy: AddCardOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [AddCard]!
  addCardsConnection(where: AddCardWhereInput, orderBy: AddCardOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): AddCardConnection!
  billingSummary(where: BillingSummaryWhereUniqueInput!): BillingSummary
  billingSummaries(where: BillingSummaryWhereInput, orderBy: BillingSummaryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [BillingSummary]!
  billingSummariesConnection(where: BillingSummaryWhereInput, orderBy: BillingSummaryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): BillingSummaryConnection!
  company(where: CompanyWhereUniqueInput!): Company
  companies(where: CompanyWhereInput, orderBy: CompanyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Company]!
  companiesConnection(where: CompanyWhereInput, orderBy: CompanyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CompanyConnection!
  inviteToJointRetro(where: InviteToJointRetroWhereUniqueInput!): InviteToJointRetro
  inviteToJointRetroes(where: InviteToJointRetroWhereInput, orderBy: InviteToJointRetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [InviteToJointRetro]!
  inviteToJointRetroesConnection(where: InviteToJointRetroWhereInput, orderBy: InviteToJointRetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): InviteToJointRetroConnection!
  plan(where: PlanWhereUniqueInput!): Plan
  plans(where: PlanWhereInput, orderBy: PlanOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Plan]!
  plansConnection(where: PlanWhereInput, orderBy: PlanOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PlanConnection!
  promoCode(where: PromoCodeWhereUniqueInput!): PromoCode
  promoCodes(where: PromoCodeWhereInput, orderBy: PromoCodeOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PromoCode]!
  promoCodesConnection(where: PromoCodeWhereInput, orderBy: PromoCodeOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PromoCodeConnection!
  retro(where: RetroWhereUniqueInput!): Retro
  retroes(where: RetroWhereInput, orderBy: RetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Retro]!
  retroesConnection(where: RetroWhereInput, orderBy: RetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RetroConnection!
  thought(where: ThoughtWhereUniqueInput!): Thought
  thoughts(where: ThoughtWhereInput, orderBy: ThoughtOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Thought]!
  thoughtsConnection(where: ThoughtWhereInput, orderBy: ThoughtOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ThoughtConnection!
  user(where: UserWhereUniqueInput!): User
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  defaultProject(where: defaultProjectWhereUniqueInput!): defaultProject
  defaultProjects(where: defaultProjectWhereInput, orderBy: defaultProjectOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [defaultProject]!
  defaultProjectsConnection(where: defaultProjectWhereInput, orderBy: defaultProjectOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): defaultProjectConnection!
  defaultTemplate(where: defaultTemplateWhereUniqueInput!): defaultTemplate
  defaultTemplates(where: defaultTemplateWhereInput, orderBy: defaultTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [defaultTemplate]!
  defaultTemplatesConnection(where: defaultTemplateWhereInput, orderBy: defaultTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): defaultTemplateConnection!
  userTemplate(where: userTemplateWhereUniqueInput!): userTemplate
  userTemplates(where: userTemplateWhereInput, orderBy: userTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [userTemplate]!
  userTemplatesConnection(where: userTemplateWhereInput, orderBy: userTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): userTemplateConnection!
  node(id: ID!): Node
}

type Retro {
  id: ID!
  userid: User!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate(where: userTemplateWhereInput, orderBy: userTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [userTemplate!]
  retroAdmin: String
  activity: String
  projectId: String
  projects(where: defaultProjectWhereInput, orderBy: defaultProjectOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [defaultProject!]
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts(where: ThoughtWhereInput, orderBy: ThoughtOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Thought!]
  published: String
  inviteToJointRetros(where: InviteToJointRetroWhereInput, orderBy: InviteToJointRetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [InviteToJointRetro!]
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type RetroConnection {
  pageInfo: PageInfo!
  edges: [RetroEdge]!
  aggregate: AggregateRetro!
}

input RetroCreateInput {
  userid: UserCreateOneWithoutRetrosInput!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateCreateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectCreateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtCreateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroCreateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroCreateManyWithoutUseridInput {
  create: [RetroCreateWithoutUseridInput!]
  connect: [RetroWhereUniqueInput!]
}

input RetroCreateManyWithoutUserTemplateInput {
  create: [RetroCreateWithoutUserTemplateInput!]
  connect: [RetroWhereUniqueInput!]
}

input RetroCreateOneWithoutInviteToJointRetrosInput {
  create: RetroCreateWithoutInviteToJointRetrosInput
  connect: RetroWhereUniqueInput
}

input RetroCreateOneWithoutThoughtsInput {
  create: RetroCreateWithoutThoughtsInput
  connect: RetroWhereUniqueInput
}

input RetroCreateWithoutInviteToJointRetrosInput {
  userid: UserCreateOneWithoutRetrosInput!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateCreateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectCreateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtCreateManyWithoutRetroidInput
  published: String
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroCreateWithoutThoughtsInput {
  userid: UserCreateOneWithoutRetrosInput!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateCreateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectCreateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  published: String
  inviteToJointRetros: InviteToJointRetroCreateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroCreateWithoutUseridInput {
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateCreateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectCreateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtCreateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroCreateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroCreateWithoutUserTemplateInput {
  userid: UserCreateOneWithoutRetrosInput!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectCreateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtCreateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroCreateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

type RetroEdge {
  node: Retro!
  cursor: String!
}

enum RetroOrderByInput {
  id_ASC
  id_DESC
  startDate_ASC
  startDate_DESC
  endDate_ASC
  endDate_DESC
  startTime_ASC
  startTime_DESC
  endTime_ASC
  endTime_DESC
  retroAdmin_ASC
  retroAdmin_DESC
  activity_ASC
  activity_DESC
  projectId_ASC
  projectId_DESC
  roomCode_ASC
  roomCode_DESC
  templateId_ASC
  templateId_DESC
  sprintnumber_ASC
  sprintnumber_DESC
  templatename_ASC
  templatename_DESC
  retrocategory1_ASC
  retrocategory1_DESC
  retrocategory2_ASC
  retrocategory2_DESC
  retrocategory3_ASC
  retrocategory3_DESC
  retrocategory4_ASC
  retrocategory4_DESC
  published_ASC
  published_DESC
  timer_ASC
  timer_DESC
  notes_ASC
  notes_DESC
  isTimerStatus_ASC
  isTimerStatus_DESC
  startedBy_ASC
  startedBy_DESC
  repeatEvery_ASC
  repeatEvery_DESC
  inputWeek_ASC
  inputWeek_DESC
  shareLink_ASC
  shareLink_DESC
  isSummary_ASC
  isSummary_DESC
  isMyRetro_ASC
  isMyRetro_DESC
  isHistory_ASC
  isHistory_DESC
  isUpComing_ASC
  isUpComing_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type RetroPreviousValues {
  id: ID!
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  retroAdmin: String
  activity: String
  projectId: String
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String!
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  published: String
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input RetroScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  startTime: String
  startTime_not: String
  startTime_in: [String!]
  startTime_not_in: [String!]
  startTime_lt: String
  startTime_lte: String
  startTime_gt: String
  startTime_gte: String
  startTime_contains: String
  startTime_not_contains: String
  startTime_starts_with: String
  startTime_not_starts_with: String
  startTime_ends_with: String
  startTime_not_ends_with: String
  endTime: String
  endTime_not: String
  endTime_in: [String!]
  endTime_not_in: [String!]
  endTime_lt: String
  endTime_lte: String
  endTime_gt: String
  endTime_gte: String
  endTime_contains: String
  endTime_not_contains: String
  endTime_starts_with: String
  endTime_not_starts_with: String
  endTime_ends_with: String
  endTime_not_ends_with: String
  retroAdmin: String
  retroAdmin_not: String
  retroAdmin_in: [String!]
  retroAdmin_not_in: [String!]
  retroAdmin_lt: String
  retroAdmin_lte: String
  retroAdmin_gt: String
  retroAdmin_gte: String
  retroAdmin_contains: String
  retroAdmin_not_contains: String
  retroAdmin_starts_with: String
  retroAdmin_not_starts_with: String
  retroAdmin_ends_with: String
  retroAdmin_not_ends_with: String
  activity: String
  activity_not: String
  activity_in: [String!]
  activity_not_in: [String!]
  activity_lt: String
  activity_lte: String
  activity_gt: String
  activity_gte: String
  activity_contains: String
  activity_not_contains: String
  activity_starts_with: String
  activity_not_starts_with: String
  activity_ends_with: String
  activity_not_ends_with: String
  projectId: String
  projectId_not: String
  projectId_in: [String!]
  projectId_not_in: [String!]
  projectId_lt: String
  projectId_lte: String
  projectId_gt: String
  projectId_gte: String
  projectId_contains: String
  projectId_not_contains: String
  projectId_starts_with: String
  projectId_not_starts_with: String
  projectId_ends_with: String
  projectId_not_ends_with: String
  roomCode: String
  roomCode_not: String
  roomCode_in: [String!]
  roomCode_not_in: [String!]
  roomCode_lt: String
  roomCode_lte: String
  roomCode_gt: String
  roomCode_gte: String
  roomCode_contains: String
  roomCode_not_contains: String
  roomCode_starts_with: String
  roomCode_not_starts_with: String
  roomCode_ends_with: String
  roomCode_not_ends_with: String
  templateId: String
  templateId_not: String
  templateId_in: [String!]
  templateId_not_in: [String!]
  templateId_lt: String
  templateId_lte: String
  templateId_gt: String
  templateId_gte: String
  templateId_contains: String
  templateId_not_contains: String
  templateId_starts_with: String
  templateId_not_starts_with: String
  templateId_ends_with: String
  templateId_not_ends_with: String
  sprintnumber: String
  sprintnumber_not: String
  sprintnumber_in: [String!]
  sprintnumber_not_in: [String!]
  sprintnumber_lt: String
  sprintnumber_lte: String
  sprintnumber_gt: String
  sprintnumber_gte: String
  sprintnumber_contains: String
  sprintnumber_not_contains: String
  sprintnumber_starts_with: String
  sprintnumber_not_starts_with: String
  sprintnumber_ends_with: String
  sprintnumber_not_ends_with: String
  templatename: String
  templatename_not: String
  templatename_in: [String!]
  templatename_not_in: [String!]
  templatename_lt: String
  templatename_lte: String
  templatename_gt: String
  templatename_gte: String
  templatename_contains: String
  templatename_not_contains: String
  templatename_starts_with: String
  templatename_not_starts_with: String
  templatename_ends_with: String
  templatename_not_ends_with: String
  retrocategory1: String
  retrocategory1_not: String
  retrocategory1_in: [String!]
  retrocategory1_not_in: [String!]
  retrocategory1_lt: String
  retrocategory1_lte: String
  retrocategory1_gt: String
  retrocategory1_gte: String
  retrocategory1_contains: String
  retrocategory1_not_contains: String
  retrocategory1_starts_with: String
  retrocategory1_not_starts_with: String
  retrocategory1_ends_with: String
  retrocategory1_not_ends_with: String
  retrocategory2: String
  retrocategory2_not: String
  retrocategory2_in: [String!]
  retrocategory2_not_in: [String!]
  retrocategory2_lt: String
  retrocategory2_lte: String
  retrocategory2_gt: String
  retrocategory2_gte: String
  retrocategory2_contains: String
  retrocategory2_not_contains: String
  retrocategory2_starts_with: String
  retrocategory2_not_starts_with: String
  retrocategory2_ends_with: String
  retrocategory2_not_ends_with: String
  retrocategory3: String
  retrocategory3_not: String
  retrocategory3_in: [String!]
  retrocategory3_not_in: [String!]
  retrocategory3_lt: String
  retrocategory3_lte: String
  retrocategory3_gt: String
  retrocategory3_gte: String
  retrocategory3_contains: String
  retrocategory3_not_contains: String
  retrocategory3_starts_with: String
  retrocategory3_not_starts_with: String
  retrocategory3_ends_with: String
  retrocategory3_not_ends_with: String
  retrocategory4: String
  retrocategory4_not: String
  retrocategory4_in: [String!]
  retrocategory4_not_in: [String!]
  retrocategory4_lt: String
  retrocategory4_lte: String
  retrocategory4_gt: String
  retrocategory4_gte: String
  retrocategory4_contains: String
  retrocategory4_not_contains: String
  retrocategory4_starts_with: String
  retrocategory4_not_starts_with: String
  retrocategory4_ends_with: String
  retrocategory4_not_ends_with: String
  published: String
  published_not: String
  published_in: [String!]
  published_not_in: [String!]
  published_lt: String
  published_lte: String
  published_gt: String
  published_gte: String
  published_contains: String
  published_not_contains: String
  published_starts_with: String
  published_not_starts_with: String
  published_ends_with: String
  published_not_ends_with: String
  timer: String
  timer_not: String
  timer_in: [String!]
  timer_not_in: [String!]
  timer_lt: String
  timer_lte: String
  timer_gt: String
  timer_gte: String
  timer_contains: String
  timer_not_contains: String
  timer_starts_with: String
  timer_not_starts_with: String
  timer_ends_with: String
  timer_not_ends_with: String
  notes: String
  notes_not: String
  notes_in: [String!]
  notes_not_in: [String!]
  notes_lt: String
  notes_lte: String
  notes_gt: String
  notes_gte: String
  notes_contains: String
  notes_not_contains: String
  notes_starts_with: String
  notes_not_starts_with: String
  notes_ends_with: String
  notes_not_ends_with: String
  isTimerStatus: String
  isTimerStatus_not: String
  isTimerStatus_in: [String!]
  isTimerStatus_not_in: [String!]
  isTimerStatus_lt: String
  isTimerStatus_lte: String
  isTimerStatus_gt: String
  isTimerStatus_gte: String
  isTimerStatus_contains: String
  isTimerStatus_not_contains: String
  isTimerStatus_starts_with: String
  isTimerStatus_not_starts_with: String
  isTimerStatus_ends_with: String
  isTimerStatus_not_ends_with: String
  startedBy: String
  startedBy_not: String
  startedBy_in: [String!]
  startedBy_not_in: [String!]
  startedBy_lt: String
  startedBy_lte: String
  startedBy_gt: String
  startedBy_gte: String
  startedBy_contains: String
  startedBy_not_contains: String
  startedBy_starts_with: String
  startedBy_not_starts_with: String
  startedBy_ends_with: String
  startedBy_not_ends_with: String
  repeatEvery: String
  repeatEvery_not: String
  repeatEvery_in: [String!]
  repeatEvery_not_in: [String!]
  repeatEvery_lt: String
  repeatEvery_lte: String
  repeatEvery_gt: String
  repeatEvery_gte: String
  repeatEvery_contains: String
  repeatEvery_not_contains: String
  repeatEvery_starts_with: String
  repeatEvery_not_starts_with: String
  repeatEvery_ends_with: String
  repeatEvery_not_ends_with: String
  inputWeek: String
  inputWeek_not: String
  inputWeek_in: [String!]
  inputWeek_not_in: [String!]
  inputWeek_lt: String
  inputWeek_lte: String
  inputWeek_gt: String
  inputWeek_gte: String
  inputWeek_contains: String
  inputWeek_not_contains: String
  inputWeek_starts_with: String
  inputWeek_not_starts_with: String
  inputWeek_ends_with: String
  inputWeek_not_ends_with: String
  shareLink: String
  shareLink_not: String
  shareLink_in: [String!]
  shareLink_not_in: [String!]
  shareLink_lt: String
  shareLink_lte: String
  shareLink_gt: String
  shareLink_gte: String
  shareLink_contains: String
  shareLink_not_contains: String
  shareLink_starts_with: String
  shareLink_not_starts_with: String
  shareLink_ends_with: String
  shareLink_not_ends_with: String
  isSummary: String
  isSummary_not: String
  isSummary_in: [String!]
  isSummary_not_in: [String!]
  isSummary_lt: String
  isSummary_lte: String
  isSummary_gt: String
  isSummary_gte: String
  isSummary_contains: String
  isSummary_not_contains: String
  isSummary_starts_with: String
  isSummary_not_starts_with: String
  isSummary_ends_with: String
  isSummary_not_ends_with: String
  isMyRetro: String
  isMyRetro_not: String
  isMyRetro_in: [String!]
  isMyRetro_not_in: [String!]
  isMyRetro_lt: String
  isMyRetro_lte: String
  isMyRetro_gt: String
  isMyRetro_gte: String
  isMyRetro_contains: String
  isMyRetro_not_contains: String
  isMyRetro_starts_with: String
  isMyRetro_not_starts_with: String
  isMyRetro_ends_with: String
  isMyRetro_not_ends_with: String
  isHistory: String
  isHistory_not: String
  isHistory_in: [String!]
  isHistory_not_in: [String!]
  isHistory_lt: String
  isHistory_lte: String
  isHistory_gt: String
  isHistory_gte: String
  isHistory_contains: String
  isHistory_not_contains: String
  isHistory_starts_with: String
  isHistory_not_starts_with: String
  isHistory_ends_with: String
  isHistory_not_ends_with: String
  isUpComing: String
  isUpComing_not: String
  isUpComing_in: [String!]
  isUpComing_not_in: [String!]
  isUpComing_lt: String
  isUpComing_lte: String
  isUpComing_gt: String
  isUpComing_gte: String
  isUpComing_contains: String
  isUpComing_not_contains: String
  isUpComing_starts_with: String
  isUpComing_not_starts_with: String
  isUpComing_ends_with: String
  isUpComing_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [RetroScalarWhereInput!]
  OR: [RetroScalarWhereInput!]
  NOT: [RetroScalarWhereInput!]
}

type RetroSubscriptionPayload {
  mutation: MutationType!
  node: Retro
  updatedFields: [String!]
  previousValues: RetroPreviousValues
}

input RetroSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: RetroWhereInput
  AND: [RetroSubscriptionWhereInput!]
  OR: [RetroSubscriptionWhereInput!]
  NOT: [RetroSubscriptionWhereInput!]
}

input RetroUpdateInput {
  userid: UserUpdateOneRequiredWithoutRetrosInput
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateUpdateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectUpdateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtUpdateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroUpdateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateManyDataInput {
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  retroAdmin: String
  activity: String
  projectId: String
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  published: String
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateManyMutationInput {
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  retroAdmin: String
  activity: String
  projectId: String
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  published: String
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateManyWithoutUseridInput {
  create: [RetroCreateWithoutUseridInput!]
  delete: [RetroWhereUniqueInput!]
  connect: [RetroWhereUniqueInput!]
  disconnect: [RetroWhereUniqueInput!]
  update: [RetroUpdateWithWhereUniqueWithoutUseridInput!]
  upsert: [RetroUpsertWithWhereUniqueWithoutUseridInput!]
  deleteMany: [RetroScalarWhereInput!]
  updateMany: [RetroUpdateManyWithWhereNestedInput!]
}

input RetroUpdateManyWithoutUserTemplateInput {
  create: [RetroCreateWithoutUserTemplateInput!]
  delete: [RetroWhereUniqueInput!]
  connect: [RetroWhereUniqueInput!]
  disconnect: [RetroWhereUniqueInput!]
  update: [RetroUpdateWithWhereUniqueWithoutUserTemplateInput!]
  upsert: [RetroUpsertWithWhereUniqueWithoutUserTemplateInput!]
  deleteMany: [RetroScalarWhereInput!]
  updateMany: [RetroUpdateManyWithWhereNestedInput!]
}

input RetroUpdateManyWithWhereNestedInput {
  where: RetroScalarWhereInput!
  data: RetroUpdateManyDataInput!
}

input RetroUpdateOneRequiredWithoutInviteToJointRetrosInput {
  create: RetroCreateWithoutInviteToJointRetrosInput
  update: RetroUpdateWithoutInviteToJointRetrosDataInput
  upsert: RetroUpsertWithoutInviteToJointRetrosInput
  connect: RetroWhereUniqueInput
}

input RetroUpdateOneRequiredWithoutThoughtsInput {
  create: RetroCreateWithoutThoughtsInput
  update: RetroUpdateWithoutThoughtsDataInput
  upsert: RetroUpsertWithoutThoughtsInput
  connect: RetroWhereUniqueInput
}

input RetroUpdateWithoutInviteToJointRetrosDataInput {
  userid: UserUpdateOneRequiredWithoutRetrosInput
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateUpdateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectUpdateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtUpdateManyWithoutRetroidInput
  published: String
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateWithoutThoughtsDataInput {
  userid: UserUpdateOneRequiredWithoutRetrosInput
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateUpdateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectUpdateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  published: String
  inviteToJointRetros: InviteToJointRetroUpdateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateWithoutUseridDataInput {
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  userTemplate: userTemplateUpdateManyWithoutRetroInput
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectUpdateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtUpdateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroUpdateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateWithoutUserTemplateDataInput {
  userid: UserUpdateOneRequiredWithoutRetrosInput
  startDate: String
  endDate: String
  startTime: String
  endTime: String
  retroAdmin: String
  activity: String
  projectId: String
  projects: defaultProjectUpdateManyInput
  roomCode: String
  templateId: String
  sprintnumber: String
  templatename: String
  retrocategory1: String
  retrocategory2: String
  retrocategory3: String
  retrocategory4: String
  thoughts: ThoughtUpdateManyWithoutRetroidInput
  published: String
  inviteToJointRetros: InviteToJointRetroUpdateManyWithoutRetroInput
  timer: String
  notes: String
  isTimerStatus: String
  startedBy: String
  repeatEvery: String
  inputWeek: String
  shareLink: String
  isSummary: String
  isMyRetro: String
  isHistory: String
  isUpComing: String
  isDeleted: String
}

input RetroUpdateWithWhereUniqueWithoutUseridInput {
  where: RetroWhereUniqueInput!
  data: RetroUpdateWithoutUseridDataInput!
}

input RetroUpdateWithWhereUniqueWithoutUserTemplateInput {
  where: RetroWhereUniqueInput!
  data: RetroUpdateWithoutUserTemplateDataInput!
}

input RetroUpsertWithoutInviteToJointRetrosInput {
  update: RetroUpdateWithoutInviteToJointRetrosDataInput!
  create: RetroCreateWithoutInviteToJointRetrosInput!
}

input RetroUpsertWithoutThoughtsInput {
  update: RetroUpdateWithoutThoughtsDataInput!
  create: RetroCreateWithoutThoughtsInput!
}

input RetroUpsertWithWhereUniqueWithoutUseridInput {
  where: RetroWhereUniqueInput!
  update: RetroUpdateWithoutUseridDataInput!
  create: RetroCreateWithoutUseridInput!
}

input RetroUpsertWithWhereUniqueWithoutUserTemplateInput {
  where: RetroWhereUniqueInput!
  update: RetroUpdateWithoutUserTemplateDataInput!
  create: RetroCreateWithoutUserTemplateInput!
}

input RetroWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userid: UserWhereInput
  startDate: String
  startDate_not: String
  startDate_in: [String!]
  startDate_not_in: [String!]
  startDate_lt: String
  startDate_lte: String
  startDate_gt: String
  startDate_gte: String
  startDate_contains: String
  startDate_not_contains: String
  startDate_starts_with: String
  startDate_not_starts_with: String
  startDate_ends_with: String
  startDate_not_ends_with: String
  endDate: String
  endDate_not: String
  endDate_in: [String!]
  endDate_not_in: [String!]
  endDate_lt: String
  endDate_lte: String
  endDate_gt: String
  endDate_gte: String
  endDate_contains: String
  endDate_not_contains: String
  endDate_starts_with: String
  endDate_not_starts_with: String
  endDate_ends_with: String
  endDate_not_ends_with: String
  startTime: String
  startTime_not: String
  startTime_in: [String!]
  startTime_not_in: [String!]
  startTime_lt: String
  startTime_lte: String
  startTime_gt: String
  startTime_gte: String
  startTime_contains: String
  startTime_not_contains: String
  startTime_starts_with: String
  startTime_not_starts_with: String
  startTime_ends_with: String
  startTime_not_ends_with: String
  endTime: String
  endTime_not: String
  endTime_in: [String!]
  endTime_not_in: [String!]
  endTime_lt: String
  endTime_lte: String
  endTime_gt: String
  endTime_gte: String
  endTime_contains: String
  endTime_not_contains: String
  endTime_starts_with: String
  endTime_not_starts_with: String
  endTime_ends_with: String
  endTime_not_ends_with: String
  userTemplate_every: userTemplateWhereInput
  userTemplate_some: userTemplateWhereInput
  userTemplate_none: userTemplateWhereInput
  retroAdmin: String
  retroAdmin_not: String
  retroAdmin_in: [String!]
  retroAdmin_not_in: [String!]
  retroAdmin_lt: String
  retroAdmin_lte: String
  retroAdmin_gt: String
  retroAdmin_gte: String
  retroAdmin_contains: String
  retroAdmin_not_contains: String
  retroAdmin_starts_with: String
  retroAdmin_not_starts_with: String
  retroAdmin_ends_with: String
  retroAdmin_not_ends_with: String
  activity: String
  activity_not: String
  activity_in: [String!]
  activity_not_in: [String!]
  activity_lt: String
  activity_lte: String
  activity_gt: String
  activity_gte: String
  activity_contains: String
  activity_not_contains: String
  activity_starts_with: String
  activity_not_starts_with: String
  activity_ends_with: String
  activity_not_ends_with: String
  projectId: String
  projectId_not: String
  projectId_in: [String!]
  projectId_not_in: [String!]
  projectId_lt: String
  projectId_lte: String
  projectId_gt: String
  projectId_gte: String
  projectId_contains: String
  projectId_not_contains: String
  projectId_starts_with: String
  projectId_not_starts_with: String
  projectId_ends_with: String
  projectId_not_ends_with: String
  projects_every: defaultProjectWhereInput
  projects_some: defaultProjectWhereInput
  projects_none: defaultProjectWhereInput
  roomCode: String
  roomCode_not: String
  roomCode_in: [String!]
  roomCode_not_in: [String!]
  roomCode_lt: String
  roomCode_lte: String
  roomCode_gt: String
  roomCode_gte: String
  roomCode_contains: String
  roomCode_not_contains: String
  roomCode_starts_with: String
  roomCode_not_starts_with: String
  roomCode_ends_with: String
  roomCode_not_ends_with: String
  templateId: String
  templateId_not: String
  templateId_in: [String!]
  templateId_not_in: [String!]
  templateId_lt: String
  templateId_lte: String
  templateId_gt: String
  templateId_gte: String
  templateId_contains: String
  templateId_not_contains: String
  templateId_starts_with: String
  templateId_not_starts_with: String
  templateId_ends_with: String
  templateId_not_ends_with: String
  sprintnumber: String
  sprintnumber_not: String
  sprintnumber_in: [String!]
  sprintnumber_not_in: [String!]
  sprintnumber_lt: String
  sprintnumber_lte: String
  sprintnumber_gt: String
  sprintnumber_gte: String
  sprintnumber_contains: String
  sprintnumber_not_contains: String
  sprintnumber_starts_with: String
  sprintnumber_not_starts_with: String
  sprintnumber_ends_with: String
  sprintnumber_not_ends_with: String
  templatename: String
  templatename_not: String
  templatename_in: [String!]
  templatename_not_in: [String!]
  templatename_lt: String
  templatename_lte: String
  templatename_gt: String
  templatename_gte: String
  templatename_contains: String
  templatename_not_contains: String
  templatename_starts_with: String
  templatename_not_starts_with: String
  templatename_ends_with: String
  templatename_not_ends_with: String
  retrocategory1: String
  retrocategory1_not: String
  retrocategory1_in: [String!]
  retrocategory1_not_in: [String!]
  retrocategory1_lt: String
  retrocategory1_lte: String
  retrocategory1_gt: String
  retrocategory1_gte: String
  retrocategory1_contains: String
  retrocategory1_not_contains: String
  retrocategory1_starts_with: String
  retrocategory1_not_starts_with: String
  retrocategory1_ends_with: String
  retrocategory1_not_ends_with: String
  retrocategory2: String
  retrocategory2_not: String
  retrocategory2_in: [String!]
  retrocategory2_not_in: [String!]
  retrocategory2_lt: String
  retrocategory2_lte: String
  retrocategory2_gt: String
  retrocategory2_gte: String
  retrocategory2_contains: String
  retrocategory2_not_contains: String
  retrocategory2_starts_with: String
  retrocategory2_not_starts_with: String
  retrocategory2_ends_with: String
  retrocategory2_not_ends_with: String
  retrocategory3: String
  retrocategory3_not: String
  retrocategory3_in: [String!]
  retrocategory3_not_in: [String!]
  retrocategory3_lt: String
  retrocategory3_lte: String
  retrocategory3_gt: String
  retrocategory3_gte: String
  retrocategory3_contains: String
  retrocategory3_not_contains: String
  retrocategory3_starts_with: String
  retrocategory3_not_starts_with: String
  retrocategory3_ends_with: String
  retrocategory3_not_ends_with: String
  retrocategory4: String
  retrocategory4_not: String
  retrocategory4_in: [String!]
  retrocategory4_not_in: [String!]
  retrocategory4_lt: String
  retrocategory4_lte: String
  retrocategory4_gt: String
  retrocategory4_gte: String
  retrocategory4_contains: String
  retrocategory4_not_contains: String
  retrocategory4_starts_with: String
  retrocategory4_not_starts_with: String
  retrocategory4_ends_with: String
  retrocategory4_not_ends_with: String
  thoughts_every: ThoughtWhereInput
  thoughts_some: ThoughtWhereInput
  thoughts_none: ThoughtWhereInput
  published: String
  published_not: String
  published_in: [String!]
  published_not_in: [String!]
  published_lt: String
  published_lte: String
  published_gt: String
  published_gte: String
  published_contains: String
  published_not_contains: String
  published_starts_with: String
  published_not_starts_with: String
  published_ends_with: String
  published_not_ends_with: String
  inviteToJointRetros_every: InviteToJointRetroWhereInput
  inviteToJointRetros_some: InviteToJointRetroWhereInput
  inviteToJointRetros_none: InviteToJointRetroWhereInput
  timer: String
  timer_not: String
  timer_in: [String!]
  timer_not_in: [String!]
  timer_lt: String
  timer_lte: String
  timer_gt: String
  timer_gte: String
  timer_contains: String
  timer_not_contains: String
  timer_starts_with: String
  timer_not_starts_with: String
  timer_ends_with: String
  timer_not_ends_with: String
  notes: String
  notes_not: String
  notes_in: [String!]
  notes_not_in: [String!]
  notes_lt: String
  notes_lte: String
  notes_gt: String
  notes_gte: String
  notes_contains: String
  notes_not_contains: String
  notes_starts_with: String
  notes_not_starts_with: String
  notes_ends_with: String
  notes_not_ends_with: String
  isTimerStatus: String
  isTimerStatus_not: String
  isTimerStatus_in: [String!]
  isTimerStatus_not_in: [String!]
  isTimerStatus_lt: String
  isTimerStatus_lte: String
  isTimerStatus_gt: String
  isTimerStatus_gte: String
  isTimerStatus_contains: String
  isTimerStatus_not_contains: String
  isTimerStatus_starts_with: String
  isTimerStatus_not_starts_with: String
  isTimerStatus_ends_with: String
  isTimerStatus_not_ends_with: String
  startedBy: String
  startedBy_not: String
  startedBy_in: [String!]
  startedBy_not_in: [String!]
  startedBy_lt: String
  startedBy_lte: String
  startedBy_gt: String
  startedBy_gte: String
  startedBy_contains: String
  startedBy_not_contains: String
  startedBy_starts_with: String
  startedBy_not_starts_with: String
  startedBy_ends_with: String
  startedBy_not_ends_with: String
  repeatEvery: String
  repeatEvery_not: String
  repeatEvery_in: [String!]
  repeatEvery_not_in: [String!]
  repeatEvery_lt: String
  repeatEvery_lte: String
  repeatEvery_gt: String
  repeatEvery_gte: String
  repeatEvery_contains: String
  repeatEvery_not_contains: String
  repeatEvery_starts_with: String
  repeatEvery_not_starts_with: String
  repeatEvery_ends_with: String
  repeatEvery_not_ends_with: String
  inputWeek: String
  inputWeek_not: String
  inputWeek_in: [String!]
  inputWeek_not_in: [String!]
  inputWeek_lt: String
  inputWeek_lte: String
  inputWeek_gt: String
  inputWeek_gte: String
  inputWeek_contains: String
  inputWeek_not_contains: String
  inputWeek_starts_with: String
  inputWeek_not_starts_with: String
  inputWeek_ends_with: String
  inputWeek_not_ends_with: String
  shareLink: String
  shareLink_not: String
  shareLink_in: [String!]
  shareLink_not_in: [String!]
  shareLink_lt: String
  shareLink_lte: String
  shareLink_gt: String
  shareLink_gte: String
  shareLink_contains: String
  shareLink_not_contains: String
  shareLink_starts_with: String
  shareLink_not_starts_with: String
  shareLink_ends_with: String
  shareLink_not_ends_with: String
  isSummary: String
  isSummary_not: String
  isSummary_in: [String!]
  isSummary_not_in: [String!]
  isSummary_lt: String
  isSummary_lte: String
  isSummary_gt: String
  isSummary_gte: String
  isSummary_contains: String
  isSummary_not_contains: String
  isSummary_starts_with: String
  isSummary_not_starts_with: String
  isSummary_ends_with: String
  isSummary_not_ends_with: String
  isMyRetro: String
  isMyRetro_not: String
  isMyRetro_in: [String!]
  isMyRetro_not_in: [String!]
  isMyRetro_lt: String
  isMyRetro_lte: String
  isMyRetro_gt: String
  isMyRetro_gte: String
  isMyRetro_contains: String
  isMyRetro_not_contains: String
  isMyRetro_starts_with: String
  isMyRetro_not_starts_with: String
  isMyRetro_ends_with: String
  isMyRetro_not_ends_with: String
  isHistory: String
  isHistory_not: String
  isHistory_in: [String!]
  isHistory_not_in: [String!]
  isHistory_lt: String
  isHistory_lte: String
  isHistory_gt: String
  isHistory_gte: String
  isHistory_contains: String
  isHistory_not_contains: String
  isHistory_starts_with: String
  isHistory_not_starts_with: String
  isHistory_ends_with: String
  isHistory_not_ends_with: String
  isUpComing: String
  isUpComing_not: String
  isUpComing_in: [String!]
  isUpComing_not_in: [String!]
  isUpComing_lt: String
  isUpComing_lte: String
  isUpComing_gt: String
  isUpComing_gte: String
  isUpComing_contains: String
  isUpComing_not_contains: String
  isUpComing_starts_with: String
  isUpComing_not_starts_with: String
  isUpComing_ends_with: String
  isUpComing_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [RetroWhereInput!]
  OR: [RetroWhereInput!]
  NOT: [RetroWhereInput!]
}

input RetroWhereUniqueInput {
  id: ID
}

type Subscription {
  addCard(where: AddCardSubscriptionWhereInput): AddCardSubscriptionPayload
  billingSummary(where: BillingSummarySubscriptionWhereInput): BillingSummarySubscriptionPayload
  company(where: CompanySubscriptionWhereInput): CompanySubscriptionPayload
  inviteToJointRetro(where: InviteToJointRetroSubscriptionWhereInput): InviteToJointRetroSubscriptionPayload
  plan(where: PlanSubscriptionWhereInput): PlanSubscriptionPayload
  promoCode(where: PromoCodeSubscriptionWhereInput): PromoCodeSubscriptionPayload
  retro(where: RetroSubscriptionWhereInput): RetroSubscriptionPayload
  thought(where: ThoughtSubscriptionWhereInput): ThoughtSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
  defaultProject(where: defaultProjectSubscriptionWhereInput): defaultProjectSubscriptionPayload
  defaultTemplate(where: defaultTemplateSubscriptionWhereInput): defaultTemplateSubscriptionPayload
  userTemplate(where: userTemplateSubscriptionWhereInput): userTemplateSubscriptionPayload
}

type Thought {
  id: ID!
  retroid: Retro!
  userid: User!
  isLike: String
  Like: String
  category: String!
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type ThoughtConnection {
  pageInfo: PageInfo!
  edges: [ThoughtEdge]!
  aggregate: AggregateThought!
}

input ThoughtCreateInput {
  retroid: RetroCreateOneWithoutThoughtsInput!
  userid: UserCreateOneWithoutThoughtsInput!
  isLike: String
  Like: String
  category: String!
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtCreateManyWithoutRetroidInput {
  create: [ThoughtCreateWithoutRetroidInput!]
  connect: [ThoughtWhereUniqueInput!]
}

input ThoughtCreateManyWithoutUseridInput {
  create: [ThoughtCreateWithoutUseridInput!]
  connect: [ThoughtWhereUniqueInput!]
}

input ThoughtCreateWithoutRetroidInput {
  userid: UserCreateOneWithoutThoughtsInput!
  isLike: String
  Like: String
  category: String!
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtCreateWithoutUseridInput {
  retroid: RetroCreateOneWithoutThoughtsInput!
  isLike: String
  Like: String
  category: String!
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

type ThoughtEdge {
  node: Thought!
  cursor: String!
}

enum ThoughtOrderByInput {
  id_ASC
  id_DESC
  isLike_ASC
  isLike_DESC
  Like_ASC
  Like_DESC
  category_ASC
  category_DESC
  thought_ASC
  thought_DESC
  groupId_ASC
  groupId_DESC
  groupName_ASC
  groupName_DESC
  isDeleted_ASC
  isDeleted_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type ThoughtPreviousValues {
  id: ID!
  isLike: String
  Like: String
  category: String!
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input ThoughtScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  isLike: String
  isLike_not: String
  isLike_in: [String!]
  isLike_not_in: [String!]
  isLike_lt: String
  isLike_lte: String
  isLike_gt: String
  isLike_gte: String
  isLike_contains: String
  isLike_not_contains: String
  isLike_starts_with: String
  isLike_not_starts_with: String
  isLike_ends_with: String
  isLike_not_ends_with: String
  Like: String
  Like_not: String
  Like_in: [String!]
  Like_not_in: [String!]
  Like_lt: String
  Like_lte: String
  Like_gt: String
  Like_gte: String
  Like_contains: String
  Like_not_contains: String
  Like_starts_with: String
  Like_not_starts_with: String
  Like_ends_with: String
  Like_not_ends_with: String
  category: String
  category_not: String
  category_in: [String!]
  category_not_in: [String!]
  category_lt: String
  category_lte: String
  category_gt: String
  category_gte: String
  category_contains: String
  category_not_contains: String
  category_starts_with: String
  category_not_starts_with: String
  category_ends_with: String
  category_not_ends_with: String
  thought: String
  thought_not: String
  thought_in: [String!]
  thought_not_in: [String!]
  thought_lt: String
  thought_lte: String
  thought_gt: String
  thought_gte: String
  thought_contains: String
  thought_not_contains: String
  thought_starts_with: String
  thought_not_starts_with: String
  thought_ends_with: String
  thought_not_ends_with: String
  groupId: String
  groupId_not: String
  groupId_in: [String!]
  groupId_not_in: [String!]
  groupId_lt: String
  groupId_lte: String
  groupId_gt: String
  groupId_gte: String
  groupId_contains: String
  groupId_not_contains: String
  groupId_starts_with: String
  groupId_not_starts_with: String
  groupId_ends_with: String
  groupId_not_ends_with: String
  groupName: String
  groupName_not: String
  groupName_in: [String!]
  groupName_not_in: [String!]
  groupName_lt: String
  groupName_lte: String
  groupName_gt: String
  groupName_gte: String
  groupName_contains: String
  groupName_not_contains: String
  groupName_starts_with: String
  groupName_not_starts_with: String
  groupName_ends_with: String
  groupName_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [ThoughtScalarWhereInput!]
  OR: [ThoughtScalarWhereInput!]
  NOT: [ThoughtScalarWhereInput!]
}

type ThoughtSubscriptionPayload {
  mutation: MutationType!
  node: Thought
  updatedFields: [String!]
  previousValues: ThoughtPreviousValues
}

input ThoughtSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: ThoughtWhereInput
  AND: [ThoughtSubscriptionWhereInput!]
  OR: [ThoughtSubscriptionWhereInput!]
  NOT: [ThoughtSubscriptionWhereInput!]
}

input ThoughtUpdateInput {
  retroid: RetroUpdateOneRequiredWithoutThoughtsInput
  userid: UserUpdateOneRequiredWithoutThoughtsInput
  isLike: String
  Like: String
  category: String
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtUpdateManyDataInput {
  isLike: String
  Like: String
  category: String
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtUpdateManyMutationInput {
  isLike: String
  Like: String
  category: String
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtUpdateManyWithoutRetroidInput {
  create: [ThoughtCreateWithoutRetroidInput!]
  delete: [ThoughtWhereUniqueInput!]
  connect: [ThoughtWhereUniqueInput!]
  disconnect: [ThoughtWhereUniqueInput!]
  update: [ThoughtUpdateWithWhereUniqueWithoutRetroidInput!]
  upsert: [ThoughtUpsertWithWhereUniqueWithoutRetroidInput!]
  deleteMany: [ThoughtScalarWhereInput!]
  updateMany: [ThoughtUpdateManyWithWhereNestedInput!]
}

input ThoughtUpdateManyWithoutUseridInput {
  create: [ThoughtCreateWithoutUseridInput!]
  delete: [ThoughtWhereUniqueInput!]
  connect: [ThoughtWhereUniqueInput!]
  disconnect: [ThoughtWhereUniqueInput!]
  update: [ThoughtUpdateWithWhereUniqueWithoutUseridInput!]
  upsert: [ThoughtUpsertWithWhereUniqueWithoutUseridInput!]
  deleteMany: [ThoughtScalarWhereInput!]
  updateMany: [ThoughtUpdateManyWithWhereNestedInput!]
}

input ThoughtUpdateManyWithWhereNestedInput {
  where: ThoughtScalarWhereInput!
  data: ThoughtUpdateManyDataInput!
}

input ThoughtUpdateWithoutRetroidDataInput {
  userid: UserUpdateOneRequiredWithoutThoughtsInput
  isLike: String
  Like: String
  category: String
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtUpdateWithoutUseridDataInput {
  retroid: RetroUpdateOneRequiredWithoutThoughtsInput
  isLike: String
  Like: String
  category: String
  thought: String
  groupId: String
  groupName: String
  isDeleted: String
}

input ThoughtUpdateWithWhereUniqueWithoutRetroidInput {
  where: ThoughtWhereUniqueInput!
  data: ThoughtUpdateWithoutRetroidDataInput!
}

input ThoughtUpdateWithWhereUniqueWithoutUseridInput {
  where: ThoughtWhereUniqueInput!
  data: ThoughtUpdateWithoutUseridDataInput!
}

input ThoughtUpsertWithWhereUniqueWithoutRetroidInput {
  where: ThoughtWhereUniqueInput!
  update: ThoughtUpdateWithoutRetroidDataInput!
  create: ThoughtCreateWithoutRetroidInput!
}

input ThoughtUpsertWithWhereUniqueWithoutUseridInput {
  where: ThoughtWhereUniqueInput!
  update: ThoughtUpdateWithoutUseridDataInput!
  create: ThoughtCreateWithoutUseridInput!
}

input ThoughtWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  retroid: RetroWhereInput
  userid: UserWhereInput
  isLike: String
  isLike_not: String
  isLike_in: [String!]
  isLike_not_in: [String!]
  isLike_lt: String
  isLike_lte: String
  isLike_gt: String
  isLike_gte: String
  isLike_contains: String
  isLike_not_contains: String
  isLike_starts_with: String
  isLike_not_starts_with: String
  isLike_ends_with: String
  isLike_not_ends_with: String
  Like: String
  Like_not: String
  Like_in: [String!]
  Like_not_in: [String!]
  Like_lt: String
  Like_lte: String
  Like_gt: String
  Like_gte: String
  Like_contains: String
  Like_not_contains: String
  Like_starts_with: String
  Like_not_starts_with: String
  Like_ends_with: String
  Like_not_ends_with: String
  category: String
  category_not: String
  category_in: [String!]
  category_not_in: [String!]
  category_lt: String
  category_lte: String
  category_gt: String
  category_gte: String
  category_contains: String
  category_not_contains: String
  category_starts_with: String
  category_not_starts_with: String
  category_ends_with: String
  category_not_ends_with: String
  thought: String
  thought_not: String
  thought_in: [String!]
  thought_not_in: [String!]
  thought_lt: String
  thought_lte: String
  thought_gt: String
  thought_gte: String
  thought_contains: String
  thought_not_contains: String
  thought_starts_with: String
  thought_not_starts_with: String
  thought_ends_with: String
  thought_not_ends_with: String
  groupId: String
  groupId_not: String
  groupId_in: [String!]
  groupId_not_in: [String!]
  groupId_lt: String
  groupId_lte: String
  groupId_gt: String
  groupId_gte: String
  groupId_contains: String
  groupId_not_contains: String
  groupId_starts_with: String
  groupId_not_starts_with: String
  groupId_ends_with: String
  groupId_not_ends_with: String
  groupName: String
  groupName_not: String
  groupName_in: [String!]
  groupName_not_in: [String!]
  groupName_lt: String
  groupName_lte: String
  groupName_gt: String
  groupName_gte: String
  groupName_contains: String
  groupName_not_contains: String
  groupName_starts_with: String
  groupName_not_starts_with: String
  groupName_ends_with: String
  groupName_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [ThoughtWhereInput!]
  OR: [ThoughtWhereInput!]
  NOT: [ThoughtWhereInput!]
}

input ThoughtWhereUniqueInput {
  id: ID
}

type User {
  id: ID!
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates(where: userTemplateWhereInput, orderBy: userTemplateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [userTemplate!]
  retros(where: RetroWhereInput, orderBy: RetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Retro!]
  thoughts(where: ThoughtWhereInput, orderBy: ThoughtOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Thought!]
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects(where: defaultProjectWhereInput, orderBy: defaultProjectOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [defaultProject!]
  isOnline: Boolean
  BillingSummary(where: BillingSummaryWhereInput, orderBy: BillingSummaryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [BillingSummary!]
  AddCard(where: AddCardWhereInput, orderBy: AddCardOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [AddCard!]
  promoCode(where: PromoCodeWhereInput, orderBy: PromoCodeOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PromoCode!]
  companyId: String
  company(where: CompanyWhereInput, orderBy: CompanyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Company!]
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type UserConnection {
  pageInfo: PageInfo!
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateOneWithoutAddCardInput {
  create: UserCreateWithoutAddCardInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutBillingSummaryInput {
  create: UserCreateWithoutBillingSummaryInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutCompanyInput {
  create: UserCreateWithoutCompanyInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutProjectsInput {
  create: UserCreateWithoutProjectsInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutRetrosInput {
  create: UserCreateWithoutRetrosInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutThoughtsInput {
  create: UserCreateWithoutThoughtsInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutUserTemplatesInput {
  create: UserCreateWithoutUserTemplatesInput
  connect: UserWhereUniqueInput
}

input UserCreateWithoutAddCardInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutBillingSummaryInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutCompanyInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutProjectsInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutRetrosInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutThoughtsInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateCreateManyWithoutUseridInput
  retros: RetroCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserCreateWithoutUserTemplatesInput {
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  retros: RetroCreateManyWithoutUseridInput
  thoughts: ThoughtCreateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectCreateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryCreateManyWithoutUserInput
  AddCard: AddCardCreateManyWithoutUserInput
  promoCode: PromoCodeCreateManyInput
  companyId: String
  company: CompanyCreateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

type UserEdge {
  node: User!
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  email_ASC
  email_DESC
  password_ASC
  password_DESC
  firstName_ASC
  firstName_DESC
  lastName_ASC
  lastName_DESC
  userStatus_ASC
  userStatus_DESC
  role_ASC
  role_DESC
  companyName_ASC
  companyName_DESC
  addressLine1_ASC
  addressLine1_DESC
  addressLine2_ASC
  addressLine2_DESC
  city_ASC
  city_DESC
  state_ASC
  state_DESC
  zipCode_ASC
  zipCode_DESC
  phoneNumber_ASC
  phoneNumber_DESC
  accountType_ASC
  accountType_DESC
  isOnline_ASC
  isOnline_DESC
  companyId_ASC
  companyId_DESC
  promoCodeIsUsed_ASC
  promoCodeIsUsed_DESC
  isEmailVerify_ASC
  isEmailVerify_DESC
  isRetroAdmin_ASC
  isRetroAdmin_DESC
  isCompanyAdmin_ASC
  isCompanyAdmin_DESC
  isGuest_ASC
  isGuest_DESC
  isDeleted_ASC
  isDeleted_DESC
  hideArchived_ASC
  hideArchived_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  email: String!
  password: String!
  firstName: String
  lastName: String
  userStatus: String
  role: String
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  isOnline: Boolean
  companyId: String
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UserWhereInput
  AND: [UserSubscriptionWhereInput!]
  OR: [UserSubscriptionWhereInput!]
  NOT: [UserSubscriptionWhereInput!]
}

type userTemplate {
  id: ID!
  userid: User!
  retro(where: RetroWhereInput, orderBy: RetroOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Retro!]
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

type userTemplateConnection {
  pageInfo: PageInfo!
  edges: [userTemplateEdge]!
  aggregate: AggregateuserTemplate!
}

input userTemplateCreateInput {
  userid: UserCreateOneWithoutUserTemplatesInput!
  retro: RetroCreateManyWithoutUserTemplateInput
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateCreateManyWithoutRetroInput {
  create: [userTemplateCreateWithoutRetroInput!]
  connect: [userTemplateWhereUniqueInput!]
}

input userTemplateCreateManyWithoutUseridInput {
  create: [userTemplateCreateWithoutUseridInput!]
  connect: [userTemplateWhereUniqueInput!]
}

input userTemplateCreateWithoutRetroInput {
  userid: UserCreateOneWithoutUserTemplatesInput!
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateCreateWithoutUseridInput {
  retro: RetroCreateManyWithoutUserTemplateInput
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

type userTemplateEdge {
  node: userTemplate!
  cursor: String!
}

enum userTemplateOrderByInput {
  id_ASC
  id_DESC
  templatename_ASC
  templatename_DESC
  category1_ASC
  category1_DESC
  category2_ASC
  category2_DESC
  category3_ASC
  category3_DESC
  category4_ASC
  category4_DESC
  isDeleted_ASC
  isDeleted_DESC
  archive_ASC
  archive_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type userTemplatePreviousValues {
  id: ID!
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
  createdAt: DateTime!
  updatedAt: DateTime!
}

input userTemplateScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  templatename: String
  templatename_not: String
  templatename_in: [String!]
  templatename_not_in: [String!]
  templatename_lt: String
  templatename_lte: String
  templatename_gt: String
  templatename_gte: String
  templatename_contains: String
  templatename_not_contains: String
  templatename_starts_with: String
  templatename_not_starts_with: String
  templatename_ends_with: String
  templatename_not_ends_with: String
  category1: String
  category1_not: String
  category1_in: [String!]
  category1_not_in: [String!]
  category1_lt: String
  category1_lte: String
  category1_gt: String
  category1_gte: String
  category1_contains: String
  category1_not_contains: String
  category1_starts_with: String
  category1_not_starts_with: String
  category1_ends_with: String
  category1_not_ends_with: String
  category2: String
  category2_not: String
  category2_in: [String!]
  category2_not_in: [String!]
  category2_lt: String
  category2_lte: String
  category2_gt: String
  category2_gte: String
  category2_contains: String
  category2_not_contains: String
  category2_starts_with: String
  category2_not_starts_with: String
  category2_ends_with: String
  category2_not_ends_with: String
  category3: String
  category3_not: String
  category3_in: [String!]
  category3_not_in: [String!]
  category3_lt: String
  category3_lte: String
  category3_gt: String
  category3_gte: String
  category3_contains: String
  category3_not_contains: String
  category3_starts_with: String
  category3_not_starts_with: String
  category3_ends_with: String
  category3_not_ends_with: String
  category4: String
  category4_not: String
  category4_in: [String!]
  category4_not_in: [String!]
  category4_lt: String
  category4_lte: String
  category4_gt: String
  category4_gte: String
  category4_contains: String
  category4_not_contains: String
  category4_starts_with: String
  category4_not_starts_with: String
  category4_ends_with: String
  category4_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  archive: String
  archive_not: String
  archive_in: [String!]
  archive_not_in: [String!]
  archive_lt: String
  archive_lte: String
  archive_gt: String
  archive_gte: String
  archive_contains: String
  archive_not_contains: String
  archive_starts_with: String
  archive_not_starts_with: String
  archive_ends_with: String
  archive_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [userTemplateScalarWhereInput!]
  OR: [userTemplateScalarWhereInput!]
  NOT: [userTemplateScalarWhereInput!]
}

type userTemplateSubscriptionPayload {
  mutation: MutationType!
  node: userTemplate
  updatedFields: [String!]
  previousValues: userTemplatePreviousValues
}

input userTemplateSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: userTemplateWhereInput
  AND: [userTemplateSubscriptionWhereInput!]
  OR: [userTemplateSubscriptionWhereInput!]
  NOT: [userTemplateSubscriptionWhereInput!]
}

input userTemplateUpdateInput {
  userid: UserUpdateOneRequiredWithoutUserTemplatesInput
  retro: RetroUpdateManyWithoutUserTemplateInput
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateUpdateManyDataInput {
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateUpdateManyMutationInput {
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateUpdateManyWithoutRetroInput {
  create: [userTemplateCreateWithoutRetroInput!]
  delete: [userTemplateWhereUniqueInput!]
  connect: [userTemplateWhereUniqueInput!]
  disconnect: [userTemplateWhereUniqueInput!]
  update: [userTemplateUpdateWithWhereUniqueWithoutRetroInput!]
  upsert: [userTemplateUpsertWithWhereUniqueWithoutRetroInput!]
  deleteMany: [userTemplateScalarWhereInput!]
  updateMany: [userTemplateUpdateManyWithWhereNestedInput!]
}

input userTemplateUpdateManyWithoutUseridInput {
  create: [userTemplateCreateWithoutUseridInput!]
  delete: [userTemplateWhereUniqueInput!]
  connect: [userTemplateWhereUniqueInput!]
  disconnect: [userTemplateWhereUniqueInput!]
  update: [userTemplateUpdateWithWhereUniqueWithoutUseridInput!]
  upsert: [userTemplateUpsertWithWhereUniqueWithoutUseridInput!]
  deleteMany: [userTemplateScalarWhereInput!]
  updateMany: [userTemplateUpdateManyWithWhereNestedInput!]
}

input userTemplateUpdateManyWithWhereNestedInput {
  where: userTemplateScalarWhereInput!
  data: userTemplateUpdateManyDataInput!
}

input userTemplateUpdateWithoutRetroDataInput {
  userid: UserUpdateOneRequiredWithoutUserTemplatesInput
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateUpdateWithoutUseridDataInput {
  retro: RetroUpdateManyWithoutUserTemplateInput
  templatename: String
  category1: String
  category2: String
  category3: String
  category4: String
  isDeleted: String
  archive: String
}

input userTemplateUpdateWithWhereUniqueWithoutRetroInput {
  where: userTemplateWhereUniqueInput!
  data: userTemplateUpdateWithoutRetroDataInput!
}

input userTemplateUpdateWithWhereUniqueWithoutUseridInput {
  where: userTemplateWhereUniqueInput!
  data: userTemplateUpdateWithoutUseridDataInput!
}

input userTemplateUpsertWithWhereUniqueWithoutRetroInput {
  where: userTemplateWhereUniqueInput!
  update: userTemplateUpdateWithoutRetroDataInput!
  create: userTemplateCreateWithoutRetroInput!
}

input userTemplateUpsertWithWhereUniqueWithoutUseridInput {
  where: userTemplateWhereUniqueInput!
  update: userTemplateUpdateWithoutUseridDataInput!
  create: userTemplateCreateWithoutUseridInput!
}

input userTemplateWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  userid: UserWhereInput
  retro_every: RetroWhereInput
  retro_some: RetroWhereInput
  retro_none: RetroWhereInput
  templatename: String
  templatename_not: String
  templatename_in: [String!]
  templatename_not_in: [String!]
  templatename_lt: String
  templatename_lte: String
  templatename_gt: String
  templatename_gte: String
  templatename_contains: String
  templatename_not_contains: String
  templatename_starts_with: String
  templatename_not_starts_with: String
  templatename_ends_with: String
  templatename_not_ends_with: String
  category1: String
  category1_not: String
  category1_in: [String!]
  category1_not_in: [String!]
  category1_lt: String
  category1_lte: String
  category1_gt: String
  category1_gte: String
  category1_contains: String
  category1_not_contains: String
  category1_starts_with: String
  category1_not_starts_with: String
  category1_ends_with: String
  category1_not_ends_with: String
  category2: String
  category2_not: String
  category2_in: [String!]
  category2_not_in: [String!]
  category2_lt: String
  category2_lte: String
  category2_gt: String
  category2_gte: String
  category2_contains: String
  category2_not_contains: String
  category2_starts_with: String
  category2_not_starts_with: String
  category2_ends_with: String
  category2_not_ends_with: String
  category3: String
  category3_not: String
  category3_in: [String!]
  category3_not_in: [String!]
  category3_lt: String
  category3_lte: String
  category3_gt: String
  category3_gte: String
  category3_contains: String
  category3_not_contains: String
  category3_starts_with: String
  category3_not_starts_with: String
  category3_ends_with: String
  category3_not_ends_with: String
  category4: String
  category4_not: String
  category4_in: [String!]
  category4_not_in: [String!]
  category4_lt: String
  category4_lte: String
  category4_gt: String
  category4_gte: String
  category4_contains: String
  category4_not_contains: String
  category4_starts_with: String
  category4_not_starts_with: String
  category4_ends_with: String
  category4_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  archive: String
  archive_not: String
  archive_in: [String!]
  archive_not_in: [String!]
  archive_lt: String
  archive_lte: String
  archive_gt: String
  archive_gte: String
  archive_contains: String
  archive_not_contains: String
  archive_starts_with: String
  archive_not_starts_with: String
  archive_ends_with: String
  archive_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [userTemplateWhereInput!]
  OR: [userTemplateWhereInput!]
  NOT: [userTemplateWhereInput!]
}

input userTemplateWhereUniqueInput {
  id: ID
}

input UserUpdateInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateManyMutationInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  isOnline: Boolean
  companyId: String
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateOneRequiredWithoutAddCardInput {
  create: UserCreateWithoutAddCardInput
  update: UserUpdateWithoutAddCardDataInput
  upsert: UserUpsertWithoutAddCardInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutBillingSummaryInput {
  create: UserCreateWithoutBillingSummaryInput
  update: UserUpdateWithoutBillingSummaryDataInput
  upsert: UserUpsertWithoutBillingSummaryInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutCompanyInput {
  create: UserCreateWithoutCompanyInput
  update: UserUpdateWithoutCompanyDataInput
  upsert: UserUpsertWithoutCompanyInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutProjectsInput {
  create: UserCreateWithoutProjectsInput
  update: UserUpdateWithoutProjectsDataInput
  upsert: UserUpsertWithoutProjectsInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutRetrosInput {
  create: UserCreateWithoutRetrosInput
  update: UserUpdateWithoutRetrosDataInput
  upsert: UserUpsertWithoutRetrosInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutThoughtsInput {
  create: UserCreateWithoutThoughtsInput
  update: UserUpdateWithoutThoughtsDataInput
  upsert: UserUpsertWithoutThoughtsInput
  connect: UserWhereUniqueInput
}

input UserUpdateOneRequiredWithoutUserTemplatesInput {
  create: UserCreateWithoutUserTemplatesInput
  update: UserUpdateWithoutUserTemplatesDataInput
  upsert: UserUpsertWithoutUserTemplatesInput
  connect: UserWhereUniqueInput
}

input UserUpdateWithoutAddCardDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutBillingSummaryDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutCompanyDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutProjectsDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutRetrosDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutThoughtsDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  userTemplates: userTemplateUpdateManyWithoutUseridInput
  retros: RetroUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpdateWithoutUserTemplatesDataInput {
  email: String
  password: String
  firstName: String
  lastName: String
  userStatus: String
  role: String
  retros: RetroUpdateManyWithoutUseridInput
  thoughts: ThoughtUpdateManyWithoutUseridInput
  companyName: String
  addressLine1: String
  addressLine2: String
  city: String
  state: String
  zipCode: Int
  phoneNumber: String
  accountType: String
  projects: defaultProjectUpdateManyWithoutCreatedByInput
  isOnline: Boolean
  BillingSummary: BillingSummaryUpdateManyWithoutUserInput
  AddCard: AddCardUpdateManyWithoutUserInput
  promoCode: PromoCodeUpdateManyInput
  companyId: String
  company: CompanyUpdateManyWithoutUserInput
  promoCodeIsUsed: String
  isEmailVerify: Boolean
  isRetroAdmin: String
  isCompanyAdmin: String
  isGuest: String
  isDeleted: String
  hideArchived: String
}

input UserUpsertWithoutAddCardInput {
  update: UserUpdateWithoutAddCardDataInput!
  create: UserCreateWithoutAddCardInput!
}

input UserUpsertWithoutBillingSummaryInput {
  update: UserUpdateWithoutBillingSummaryDataInput!
  create: UserCreateWithoutBillingSummaryInput!
}

input UserUpsertWithoutCompanyInput {
  update: UserUpdateWithoutCompanyDataInput!
  create: UserCreateWithoutCompanyInput!
}

input UserUpsertWithoutProjectsInput {
  update: UserUpdateWithoutProjectsDataInput!
  create: UserCreateWithoutProjectsInput!
}

input UserUpsertWithoutRetrosInput {
  update: UserUpdateWithoutRetrosDataInput!
  create: UserCreateWithoutRetrosInput!
}

input UserUpsertWithoutThoughtsInput {
  update: UserUpdateWithoutThoughtsDataInput!
  create: UserCreateWithoutThoughtsInput!
}

input UserUpsertWithoutUserTemplatesInput {
  update: UserUpdateWithoutUserTemplatesDataInput!
  create: UserCreateWithoutUserTemplatesInput!
}

input UserWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  firstName: String
  firstName_not: String
  firstName_in: [String!]
  firstName_not_in: [String!]
  firstName_lt: String
  firstName_lte: String
  firstName_gt: String
  firstName_gte: String
  firstName_contains: String
  firstName_not_contains: String
  firstName_starts_with: String
  firstName_not_starts_with: String
  firstName_ends_with: String
  firstName_not_ends_with: String
  lastName: String
  lastName_not: String
  lastName_in: [String!]
  lastName_not_in: [String!]
  lastName_lt: String
  lastName_lte: String
  lastName_gt: String
  lastName_gte: String
  lastName_contains: String
  lastName_not_contains: String
  lastName_starts_with: String
  lastName_not_starts_with: String
  lastName_ends_with: String
  lastName_not_ends_with: String
  userStatus: String
  userStatus_not: String
  userStatus_in: [String!]
  userStatus_not_in: [String!]
  userStatus_lt: String
  userStatus_lte: String
  userStatus_gt: String
  userStatus_gte: String
  userStatus_contains: String
  userStatus_not_contains: String
  userStatus_starts_with: String
  userStatus_not_starts_with: String
  userStatus_ends_with: String
  userStatus_not_ends_with: String
  role: String
  role_not: String
  role_in: [String!]
  role_not_in: [String!]
  role_lt: String
  role_lte: String
  role_gt: String
  role_gte: String
  role_contains: String
  role_not_contains: String
  role_starts_with: String
  role_not_starts_with: String
  role_ends_with: String
  role_not_ends_with: String
  userTemplates_every: userTemplateWhereInput
  userTemplates_some: userTemplateWhereInput
  userTemplates_none: userTemplateWhereInput
  retros_every: RetroWhereInput
  retros_some: RetroWhereInput
  retros_none: RetroWhereInput
  thoughts_every: ThoughtWhereInput
  thoughts_some: ThoughtWhereInput
  thoughts_none: ThoughtWhereInput
  companyName: String
  companyName_not: String
  companyName_in: [String!]
  companyName_not_in: [String!]
  companyName_lt: String
  companyName_lte: String
  companyName_gt: String
  companyName_gte: String
  companyName_contains: String
  companyName_not_contains: String
  companyName_starts_with: String
  companyName_not_starts_with: String
  companyName_ends_with: String
  companyName_not_ends_with: String
  addressLine1: String
  addressLine1_not: String
  addressLine1_in: [String!]
  addressLine1_not_in: [String!]
  addressLine1_lt: String
  addressLine1_lte: String
  addressLine1_gt: String
  addressLine1_gte: String
  addressLine1_contains: String
  addressLine1_not_contains: String
  addressLine1_starts_with: String
  addressLine1_not_starts_with: String
  addressLine1_ends_with: String
  addressLine1_not_ends_with: String
  addressLine2: String
  addressLine2_not: String
  addressLine2_in: [String!]
  addressLine2_not_in: [String!]
  addressLine2_lt: String
  addressLine2_lte: String
  addressLine2_gt: String
  addressLine2_gte: String
  addressLine2_contains: String
  addressLine2_not_contains: String
  addressLine2_starts_with: String
  addressLine2_not_starts_with: String
  addressLine2_ends_with: String
  addressLine2_not_ends_with: String
  city: String
  city_not: String
  city_in: [String!]
  city_not_in: [String!]
  city_lt: String
  city_lte: String
  city_gt: String
  city_gte: String
  city_contains: String
  city_not_contains: String
  city_starts_with: String
  city_not_starts_with: String
  city_ends_with: String
  city_not_ends_with: String
  state: String
  state_not: String
  state_in: [String!]
  state_not_in: [String!]
  state_lt: String
  state_lte: String
  state_gt: String
  state_gte: String
  state_contains: String
  state_not_contains: String
  state_starts_with: String
  state_not_starts_with: String
  state_ends_with: String
  state_not_ends_with: String
  zipCode: Int
  zipCode_not: Int
  zipCode_in: [Int!]
  zipCode_not_in: [Int!]
  zipCode_lt: Int
  zipCode_lte: Int
  zipCode_gt: Int
  zipCode_gte: Int
  phoneNumber: String
  phoneNumber_not: String
  phoneNumber_in: [String!]
  phoneNumber_not_in: [String!]
  phoneNumber_lt: String
  phoneNumber_lte: String
  phoneNumber_gt: String
  phoneNumber_gte: String
  phoneNumber_contains: String
  phoneNumber_not_contains: String
  phoneNumber_starts_with: String
  phoneNumber_not_starts_with: String
  phoneNumber_ends_with: String
  phoneNumber_not_ends_with: String
  accountType: String
  accountType_not: String
  accountType_in: [String!]
  accountType_not_in: [String!]
  accountType_lt: String
  accountType_lte: String
  accountType_gt: String
  accountType_gte: String
  accountType_contains: String
  accountType_not_contains: String
  accountType_starts_with: String
  accountType_not_starts_with: String
  accountType_ends_with: String
  accountType_not_ends_with: String
  projects_every: defaultProjectWhereInput
  projects_some: defaultProjectWhereInput
  projects_none: defaultProjectWhereInput
  isOnline: Boolean
  isOnline_not: Boolean
  BillingSummary_every: BillingSummaryWhereInput
  BillingSummary_some: BillingSummaryWhereInput
  BillingSummary_none: BillingSummaryWhereInput
  AddCard_every: AddCardWhereInput
  AddCard_some: AddCardWhereInput
  AddCard_none: AddCardWhereInput
  promoCode_every: PromoCodeWhereInput
  promoCode_some: PromoCodeWhereInput
  promoCode_none: PromoCodeWhereInput
  companyId: String
  companyId_not: String
  companyId_in: [String!]
  companyId_not_in: [String!]
  companyId_lt: String
  companyId_lte: String
  companyId_gt: String
  companyId_gte: String
  companyId_contains: String
  companyId_not_contains: String
  companyId_starts_with: String
  companyId_not_starts_with: String
  companyId_ends_with: String
  companyId_not_ends_with: String
  company_every: CompanyWhereInput
  company_some: CompanyWhereInput
  company_none: CompanyWhereInput
  promoCodeIsUsed: String
  promoCodeIsUsed_not: String
  promoCodeIsUsed_in: [String!]
  promoCodeIsUsed_not_in: [String!]
  promoCodeIsUsed_lt: String
  promoCodeIsUsed_lte: String
  promoCodeIsUsed_gt: String
  promoCodeIsUsed_gte: String
  promoCodeIsUsed_contains: String
  promoCodeIsUsed_not_contains: String
  promoCodeIsUsed_starts_with: String
  promoCodeIsUsed_not_starts_with: String
  promoCodeIsUsed_ends_with: String
  promoCodeIsUsed_not_ends_with: String
  isEmailVerify: Boolean
  isEmailVerify_not: Boolean
  isRetroAdmin: String
  isRetroAdmin_not: String
  isRetroAdmin_in: [String!]
  isRetroAdmin_not_in: [String!]
  isRetroAdmin_lt: String
  isRetroAdmin_lte: String
  isRetroAdmin_gt: String
  isRetroAdmin_gte: String
  isRetroAdmin_contains: String
  isRetroAdmin_not_contains: String
  isRetroAdmin_starts_with: String
  isRetroAdmin_not_starts_with: String
  isRetroAdmin_ends_with: String
  isRetroAdmin_not_ends_with: String
  isCompanyAdmin: String
  isCompanyAdmin_not: String
  isCompanyAdmin_in: [String!]
  isCompanyAdmin_not_in: [String!]
  isCompanyAdmin_lt: String
  isCompanyAdmin_lte: String
  isCompanyAdmin_gt: String
  isCompanyAdmin_gte: String
  isCompanyAdmin_contains: String
  isCompanyAdmin_not_contains: String
  isCompanyAdmin_starts_with: String
  isCompanyAdmin_not_starts_with: String
  isCompanyAdmin_ends_with: String
  isCompanyAdmin_not_ends_with: String
  isGuest: String
  isGuest_not: String
  isGuest_in: [String!]
  isGuest_not_in: [String!]
  isGuest_lt: String
  isGuest_lte: String
  isGuest_gt: String
  isGuest_gte: String
  isGuest_contains: String
  isGuest_not_contains: String
  isGuest_starts_with: String
  isGuest_not_starts_with: String
  isGuest_ends_with: String
  isGuest_not_ends_with: String
  isDeleted: String
  isDeleted_not: String
  isDeleted_in: [String!]
  isDeleted_not_in: [String!]
  isDeleted_lt: String
  isDeleted_lte: String
  isDeleted_gt: String
  isDeleted_gte: String
  isDeleted_contains: String
  isDeleted_not_contains: String
  isDeleted_starts_with: String
  isDeleted_not_starts_with: String
  isDeleted_ends_with: String
  isDeleted_not_ends_with: String
  hideArchived: String
  hideArchived_not: String
  hideArchived_in: [String!]
  hideArchived_not_in: [String!]
  hideArchived_lt: String
  hideArchived_lte: String
  hideArchived_gt: String
  hideArchived_gte: String
  hideArchived_contains: String
  hideArchived_not_contains: String
  hideArchived_starts_with: String
  hideArchived_not_starts_with: String
  hideArchived_ends_with: String
  hideArchived_not_ends_with: String
  createdAt: DateTime
  createdAt_not: DateTime
  createdAt_in: [DateTime!]
  createdAt_not_in: [DateTime!]
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  updatedAt: DateTime
  updatedAt_not: DateTime
  updatedAt_in: [DateTime!]
  updatedAt_not_in: [DateTime!]
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  AND: [UserWhereInput!]
  OR: [UserWhereInput!]
  NOT: [UserWhereInput!]
}

input UserWhereUniqueInput {
  id: ID
  email: String
}
`
      }
    